<?php

/**
 * ReserveController.
 *
 */
class IndexController extends Controller
{
    //protected $auth_actions = array('index', 'signout', 'follow');

    public function DefaultAction()
    {
        return $this->Twigrender(array(),"Index/Index");
    }


    public function AjaxAction(){

        if(!is_null($this->request->getPost('job'))){
            if($this->request->getPost('job') == "getm"){

                $this->getMonster();

            }elseif($this->request->getPost('job') == "gets"){

                $this->getSkill();

            }elseif($this->request->getPost('job') == "getl"){

                $this->getLeader();

            }elseif($this->request->getPost('job') == "geta"){

                $this->getArousal();

            }elseif($this->request->getPost('job') == "setm"){

            }elseif($this->request->getPost('job') == "sets"){

            }elseif($this->request->getPost('job') == "setl"){
                $this->get();

            }elseif($this->request->getPost('job') == "seta"){
                $this->get();

            }elseif($this->request->getPost('job') == "dmap"){
                $this->getDropmap();

            }elseif($this->request->getPost('job') == "evol"){
                $this->getEvolution();
            }

        }
    }


    public function getMonster(){
        if($this->request->getPost('id') == "all"){
            $data = $this->db_manager->get('Monster')
                                ->fetchAllMonster();
        }else{
            $data = $this->db_manager->get('Monster')
            ->fetchMonsterId(
                array("id" => $this->request->getPost('id'))
            );
        }

        $this->json($data);
    }

    public function getSkill(){
        if($this->request->getPost('id') == "all"){
            $data = $this->db_manager->get('Skill')
            ->fetchAllSkill();
        }else{
            $data = $this->db_manager->get('Skill')
            ->fetchSkillId(
                    array("id" => $this->request->getPost('id'))
            );
        }

        $this->json($data);
    }


    public function getLeader(){
        if($this->request->getPost('id') == "all"){
            $data = $this->db_manager->get('Reader')->fetchAllReader();
        }else{
            $data = $this->db_manager->get('Reader')->fetchReaderId(
                    array("id" => $this->request->getPost('id'))
            );
        }

        $this->json($data);
    }

    public function getArousal(){
        if($this->request->getPost('id') == "all"){
            $data = $this->db_manager->get('Arousal')
            ->fetchAllArousal();
        }else{
            $data = $this->db_manager->get('Arousal')
            ->fetchArousalId(
                    array("id" => $this->request->getPost('id'))
            );
        }

        $this->json($data);
    }

    public function getDropmap(){
        if($this->request->getPost('id') == "all"){
            $data = $this->db_manager->get('Dropmap')
            ->fetchAllDropmap();
        }else{
            $data = $this->db_manager->get('Dropmap')
            ->fetchDropmapId(
                    array("id" => $this->request->getPost('id'))
            );
        }

        $this->json($data);
    }
    public function getEvolution(){
        if($this->request->getPost('id') == "all"){
            $data = $this->db_manager->get('Evolution')
            ->fetchAllEvolution();
        }else{
            $data = $this->db_manager->get('Evolution')
            ->fetchEvolutionId(
                    array("id" => $this->request->getPost('id'))
            );
        }

        $this->json($data);
    }
}
