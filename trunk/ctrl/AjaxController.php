<?php

/**
 * AjaxController.
 *
 */
class AjaxController extends Controller
{

    public function mainAction()
    {

        if(!is_null($this->request->getPost('job'))){
            if($this->request->getPost('job') == "monthAll"){

                $this->monthAll();

            }elseif($this->request->getPost('job') == "reserveRoom"){

                $this->reserveRoom();

            }elseif($this->request->getPost('job') == "getdate"){

                $this->getdate();

            }elseif($this->request->getPost('job') == "getReserve"){

                $this->getReserve();

            }elseif($this->request->getPost('job') == ""){

            }elseif($this->request->getPost('job') == "table"){

            /*
             * 予約情報の取得
             */
            }elseif($this->request->getPost('job') == "get"){
                $this->get();
            }

        }

    }

    /**********************************
     **********************************
     *
     *
     *
     **********************************
     **********************************/

    public function monthAll(){


        if(
            $this->request->getGet('yyyy') == NULL
            || $this->request->getGet('yyyy') == "")
        {
            //$releace = day;
            //$target = 1;
        }else{
            $releace = $this->request->getGet("releace");
            $target = $this->request->getGet("target");
        }

        $_link = new Room();
        $_link->target = 1;
        $link_back = $_link->get_all_room(0);

        $roomlist = $link_back["roomlist"];
        $dayornight = $link_back["dayornight"];
        $res = $this->db_manager->get('Roomname')->fetchNumberRoomname($link_back["number"]);


        return $this->json($res);
    }

    /**********************************
     **********************************
    *
    *
    *
    **********************************
    **********************************/

    public function reserveRoom(){
                if(
                    $this->request->getPost('year') == NULL
                    || $this->request->getPost('year') == "")
                {
                    $mm = date("m");
                    $yyyy = date("Y");
                    //$releace = day;
                    //$target = 1;
                }else{
                    $mm = $this->request->getPost("month");
                    $yyyy = $this->request->getPost("year");
                    //$releace = $this->request->getPost("releace");
                    //$target = $this->request->getPost("target");
                }

                //予約チェックインスタンス作成
                $_callender_check = new YoyakuChk();
                $_callender_check->db_manager = $this->db_manager;
                $_callender_check->View_target = 1;

                //月末日、曜日のセット
                $lastd = date('t',strtotime($yyyy."-". $mm."-1"));
                $week_day = date('N',strtotime($yyyy."-". $mm."-1"));

                //カレンダー（予約部分）インスタンス作成
                $callender = new Callender();
                $callender->yoyaku_chk = $_callender_check;

                /*
                 *画面描画
                */
                $reserved = $callender->show_callender($this->request->getPost('roomid'),$lastd,$week_day,$yyyy,$mm);

                return $this->json($reserved);
    }

    /**********************************
     **********************************
    *
    *
    *
    **********************************
    **********************************/

    public function get(){
        if($this->request->getPost('menu') == "reserveSingle"){

            $res = $this->db_manager
                ->get('Yoyaku')->fetchRoomTimeYoyaku(
                    array(
                        "Start"=>$this->request->getPost('date'),
                        "End"=>$this->request->getPost('date'),
                        "roomid"=>$this->request->getPost('id')
                    ));

            if(count($res) == 0){

                return $this->json("false");

            }else{

                return $this->json($res);
            }


        }elseif($this->request->getPost('menu') == "update"){

        }elseif ($this->request->getPost('menu') == "delete"){

        }
    }

    /**********************************
     **********************************
    *
    *
    *
    **********************************
    **********************************/

    public function getdate(){

        $date = ($this->request->getPost("date") == "")?date("d"):$this->request->getPost("date");
        $month =   ($this->request->getPost("month") == "")?date("m"):$this->request->getPost("month");
        $year = ($this->request->getPost("year") == "")?date("y"):$this->request->getPost("year");

        $DateMaker = new DateMaker($year,$month,$date);

        if($this->request->getPost('menu') == "month"){

            return $this->json($DateMaker->month());

        }elseif($this->request->getPost('menu') == "week"){

        }elseif($this->request->getPost('menu') == "2week"){

        }elseif ($this->request->getPost('menu') == "one"){

        }
    }

    /**********************************
     **********************************
    *
    * 予約データ取得
    *
    **********************************
    **********************************/

    public function getReserve(){
        if(
            $this->request->getGet('hiduke') != NULL
            || $this->request->getGet('hiduke') != "")
        {
            $date = $this->request->getGet("hiduke");
            $roomid = $this->request->getGet("roomid");
        }

        $res = $this->db_manager->get('Yoyaku')
                ->fetchRoomTimeYoyaku(
                    array(
                        "Start"  => $date,
                        "End"    => $date,
                        "roomid" => $roomid
                    ));
                    var_dump($res);
        $this->json($res);
    }

    /**********************************
     **********************************
    *
    * 部屋処理
    *
    **********************************
    **********************************/
    public function room(){
        if($this->request->getPost('menu') == "insert"){

        }elseif($this->request->getPost('menu') == "update"){

        }elseif ($this->request->getPost('menu') == "delete"){

        }
    }

    /**********************************
     **********************************
    *
    * 注文処理
    *
    **********************************
    **********************************/

    public function order(){
        if($this->request->getPost('menu') == "insert"){

        }elseif($this->request->getPost('menu') == "update"){

        }elseif ($this->request->getPost('menu') == "delete"){

        }
    }

    /**********************************
     **********************************
    *
    * 注文履歴処理
    *
    **********************************
    **********************************/

    public function orderHistroy(){
        if($this->request->getPost('menu') == "insert"){

        }elseif($this->request->getPost('menu') == "update"){

        }elseif ($this->request->getPost('menu') == "delete"){

        }
    }

}
