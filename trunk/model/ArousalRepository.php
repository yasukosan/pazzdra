<?php

class ArousalRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"		    =>array("id","int","0",1),
            "icon"			=>array("icon","int","0",1),
            "Name"			=>array("Name","text","",1),
            "Type"			=>array("Type","int","0",1),
            "Program"		=>array("Program","int","0",1),
            "Propatie"		=>array("Propatie","text","0",1),
            "Description"	=>array("Description","text","0",1),
            "etc3"			=>array("etc3","text","",1),
            "etc4"			=>array("etc4","text","",1)
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO arousal(
                id,icon,Name,Type,Program,Propatie,Description,etc3,etc4
                )
            VALUES(
                :id,:icon,:Name,:Type,:Program,:Propatie,:Description,:etc3,:etc4
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':icon'			=> $status['icon'],
                    ':Name'			=> $status['Name'],
                    ':Type'			=> $status['Type'],
                    ':Program'		=> $status['Program'],
                    ':Propatie'		=> $status['Propatie'],
                    ':Description'	=> $status['Description'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE arousal SET
            icon			= :icon,
            Name			= :Name,
            Type			= :Type,
            Program			= :Program,
            Propatie		= :Propatie,
            Description		= :Description,
            etc3			= :etc3,
            etc4			= :etc4
        WHERE
            id		= :id
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':icon'			=> $status['icon'],
                    ':Name'			=> $status['Name'],
                    ':Type'			=> $status['Type'],
                    ':Program'		=> $status['Program'],
                    ':Propatie'		=> $status['Propatie'],
                    ':Description'	=> $status['Description'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
        ));
    }

    public function fetchAllArousal(){
        $sql = "
        SELECT
            id,
            icon,
            Name,
            Type,
            Program,
            Propatie,
            Description,
            etc3,
            etc4
        FROM
            arousal
        ";

        return $this->fetchAll($sql,array());
    }


    public function fetchArousalId($status){
        $sql = "
        SELECT
            id,
            icon,
            Name,
            Type,
            Program,
            Propatie,
            Description,
            etc3,
            etc4
        FROM
            arousal
        WHERE
            id		= :id
        ";

        return $this->fetch($sql,array(
                    ':id'			=> $status['id'],
        ));
    }

}

