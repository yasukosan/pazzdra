<?php

class MonsterRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"		    =>array("id","int","0",1),
            "Name"			=>array("Name","text","",1),
            "Ruby"			=>array("Ruby","int","",1),
            "Attribute"		=>array("Attribute","int","0",1),
            "SubAttribute"	=>array("SubAttribute","int","0",1),
            "Rarity"		=>array("Rarity","int","0",1),
            "Cost"			=>array("Cost","int","0",1),
            "Type"			=>array("Type","int","0",1),
            "SubType"		=>array("SubType","int","0",1),
            "ThirdType"		=>array("ThirdType","int","0",1),
            "HP"			=>array("HP","int","0",1),
            "Attack"		=>array("Attack","int","0",1),
            "Cure"			=>array("Cure","int","0",1),
            "Skill"			=>array("Skill","int","0",1),
            "Reader"		=>array("Reader","int","0",1),
            "Arousal"		=>array("Arousal","text","0",1),
            "Evolution"		=>array("Evolution","text","0",1),
            "etc3"			=>array("etc3","text","",1),
            "etc4"			=>array("etc4","text","",1),
    		"Series"		=>array("Series","int","0",1),
    		"EvoBefor"		=>array("EvoBefor","int","0",1),
    		"EvoMaterial"	=>array("EvoMaterial","text","0",1),
    		"EvoType"		=>array("EvoType","int","0",1)
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO monster(
                id,Name,Ruby,Attribute,SubAttribute,Rarity,Cost,
                Type,SubType,ThirdType,HP,Attack,Cure,Skill,Reader,Arousal,
                Evolution,etc3,etc4
                )
            VALUES(
                :id,:Name,:Ruby,:Attribute,:SubAttribute,:Rarity,:Cost,
                :Type,:SubType,:ThirdType,:HP,:Attack,:Cure,:Skill,:Reader,:Arousal,
                :Evolution,:etc3,:etc4
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Ruby'			=> $status['Ruby'],
                    ':Attribute'	=> $status['Attribute'],
                    ':SubAttribute'	=> $status['SubAttribute'],
                    ':Rarity'		=> $status['Rarity'],
                    ':Cost'			=> $status['Cost'],
                    ':Type'			=> $status['Type'],
                    ':SubType'		=> $status['SubType'],
                    ':ThirdType'	=> $status['ThirdType'],
                    ':HP'			=> $status['HP'],
                    ':Attack'		=> $status['Attack'],
                    ':Cure'			=> $status['Cure'],
                    ':Skill'		=> $status['Skill'],
                    ':Reader'		=> $status['Reader'],
                    ':Arousal'		=> $status['Arousal'],
                    ':Evolution'	=> $status['Evolution'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4'],
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE monster SET
            Name			=> :Name,
            Ruby			=> :Ruby,
            Attribute		=> :Attribute,
            SubAttribute	=> :SubAttribute,
            Rarity			=> :Rarity,
            Cost			=> :Cost,
            Type			=> :Type,
            SubType			=> :SubType,
            ThirdType		=> :ThirdType,
            HP				=> :HP,
            Attack			=> :Attack,
            Cure			=> :Cure,
            Skill			=> :Skill,
            Reader			=> :Reader,
            Arousal			=> :Arousal,
            Evolution		=> :Evolution,
            etc3			=> :etc3,
            etc4			=> :etc4
        WHERE
            id				=> :id,
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Ruby'			=> $status['Ruby'],
                    ':Attribute'	=> $status['Attribute'],
                    ':SubAttribute'	=> $status['SubAttribute'],
                    ':Rarity'		=> $status['Rarity'],
                    ':Cost'			=> $status['Cost'],
                    ':Type'			=> $status['Type'],
                    ':SubType'		=> $status['SubType'],
                    ':ThirdType'	=> $status['ThirdType'],
                    ':HP'			=> $status['HP'],
                    ':Attack'		=> $status['Attack'],
                    ':Cure'			=> $status['Cure'],
                    ':Skill'		=> $status['Skill'],
                    ':Reader'		=> $status['Reader'],
                    ':Arousal'		=> $status['Arousal'],
                    ':Evolution'	=> $status['Evolution'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4'],
        ));
    }

    public function fetchAllMonster(){
        $sql = "
        SELECT
            id,Name,Ruby,Attribute,SubAttribute,Rarity,Cost,
            Type,SubType,ThirdType,HP,Attack,Cure,Skill,Reader,Arousal,
            Evolution,etc3,etc4,Series,EvoBefor,EvoMaterial,EvoType
        FROM
            monster
        ";

        return $this->fetchAll($sql,array());
    }

    public function fetchMonsterId(){
        $sql = "
        SELECT
            id,Name,Ruby,Attribute,SubAttribute,Rarity,Cost,
            Type,SubType,ThirdType,HP,Attack,Cure,Skill,Reader,Arousal,
            Evolution,etc3,etc4,Series,EvoBefor,EvoMaterial,EvoType
        FROM
            monster
        WHERE
            id = :id
        ";
        return $this->fetch($sql,array(':id' => $id));
    }

}

