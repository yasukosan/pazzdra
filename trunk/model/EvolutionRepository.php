<?php

class EvolutionRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"	    =>array("id","int","0",1),
            "tmp"		=>array("tmp","text","",1),
            "eb"		=>array("eb","int","0",1),
            "em1"		=>array("em1","text","0",1),
            "eam1"		=>array("eam1","int","0",1),
            "em2"		=>array("em2","text","0",1),
            "eam2"		=>array("eam2","int","0",1),
            "em3"		=>array("em3","text","0",1),
            "eam3"		=>array("eam3","int","0",1),
            "em4"		=>array("em4","text","0",1),
            "eam4"		=>array("eam4","int","0",1),
            "em5"		=>array("em5","text","0",1),
            "eam5"		=>array("eam5","int","0",1),
            "em6"		=>array("em6","text","0",1),
            "eam6"		=>array("eam6","int","0",1)
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO evolution(
                id,tmp,eb,em1,eam1,em2,eam2,em3,eam3,em4,eam4,em5,eam5,em6,eam6
                )
            VALUES(
                :id,:tmp,:eb,:em1,:eam1,:em2,:eam2,:em3,:eam3,:em4,:eam4,:em5,:eam5,:em6,:eam6
            )
        ";

        $stmt = $this->execute($sql,array(
                ':id'		=> $status['id'],
                ':tmp'		=> $status['tmp'],
                ':eb'		=> $status['eb'],
                ':em1'		=> $status['em1'],
                ':eam1'		=> $status['eam1'],
                ':em2'		=> $status['em2'],
                ':eam2'		=> $status['eam2'],
                ':em3'		=> $status['em3'],
                ':eam3'		=> $status['eam3'],
                ':em4'		=> $status['em4'],
                ':eam4'		=> $status['eam4'],
                ':em5'		=> $status['em5'],
                ':eam5'		=> $status['eam5'],
                ':em6'		=> $status['em6'],
                ':eam6'		=> $status['eam6']
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE evolution SET
            id		= :id,
            tmp		= :tmp,
            eb		= :eb,
            em1		= :em1,
            eam1	= :eam1,
            em2		= :em2,
            eam2	= :eam2,
            em3		= :em3,
            eam3	= :eam3,
            em4		= :em4,
            eam4	= :eam4,
            em5		= :em5,
            eam5	= :eam5,
            em6		= :em6,
            eam6	= :eam6
        WHERE
            id			= :id
        ";

        $stmt = $this->execute($sql,array(
                ':id'		=> $status['id'],
                ':tmp'		=> $status['tmp'],
                ':eb'		=> $status['eb'],
                ':em1'		=> $status['em1'],
                ':eam1'		=> $status['eam1'],
                ':em2'		=> $status['em2'],
                ':eam2'		=> $status['eam2'],
                ':em3'		=> $status['em3'],
                ':eam3'		=> $status['eam3'],
                ':em4'		=> $status['em4'],
                ':eam4'		=> $status['eam4'],
                ':em5'		=> $status['em5'],
                ':eam5'		=> $status['eam5'],
                ':em6'		=> $status['em6'],
                ':eam6'		=> $status['eam6']
        ));
    }

    public function fetchAllEvolution(){
        $sql = "
        SELECT
            id,
            tmp,
            eb,
            em1,
            eam1,
            em2,
            eam2,
            em3,
            eam3,
            em4,
            eam4,
            em5,
            eam5,
            em6,
            eam6
        FROM
            evolution

        ";

        return $this->fetchAll($sql,array());
    }


    public function fetchEvolutionId(){
        $sql = "
        SELECT
            id,
            tmp,
            eb,
            em1,
            eam1,
            em2,
            eam2,
            em3,
            eam3,
            em4,
            eam4,
            em5,
            eam5,
            em6,
            eam6
        FROM
            evolution
        WHERE
            id		= :id
        ";

        return $this->fetch($sql,array(
                ':id'			=> $status['id'],
        ));
    }

}

