<?php

class DropmapRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"		    =>array("id","int","0",1),
            "bchenge"		=>array("bchenge","text","",1),
            "achenge"		=>array("achenge","text","0",1),
            "type"			=>array("type","int","0",1),
            "etc3"			=>array("etc3","text","",1),
            "etc4"			=>array("etc4","text","",1)
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO dropmap(
                id,achenge,bchenge,type,etc3,etc4
                )
            VALUES(
                :id,:achenge,:bchenge,:type,:etc3,:etc4
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':bchenge'			=> $status['bchenge'],
                    ':achenge'		=> $status['achenge'],
                    ':type'			=> $status['type'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE dropmap SET
            bchenge		= :bchenge,
            achenge		= :achenge,
            type		= :type,
            etc3		= :etc3,
            etc4		= :etc4
        WHERE
            id			= :id
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':bchenge'		=> $status['bchenge'],
                    ':achenge'		=> $status['achenge'],
                    ':type'			=> $status['type'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
        ));
    }

    public function fetchAllDropmap(){
        $sql = "
        SELECT
            id,
            bchenge,
            achenge,
            type,
            etc3,
            etc4
        FROM
            dropmap

        ";

        return $this->fetchAll($sql,array());
    }


    public function fetchDropmapId(){
        $sql = "
        SELECT
            id,
            bchenge,
            achenge,
            type,
            etc3,
            etc4
        FROM
            dropmap
        WHERE
            id		= :id
        ";

        return $this->fetch($sql,array(
                ':id'			=> $status['id'],
        ));
    }

}

