<?php

class SkillRepository extends DbRepository
{

    var $scheem = "";
    var $layout = array(
            "id"		    =>array("id","int","0",1),
            "Name"			=>array("Name","text","",1),
            "Start"			=>array("Start","int","0",1),
            "Max"			=>array("Max","text","0",1),
            "MaxLevel"		=>array("MaxLevel","int","0",1),
            "Type"			=>array("Type","text","0",1),
    		"Attribute"		=>array("Attribute","text","0",1),
    		"MType"			=>array("MType","text","0",1),
            "Target"		=>array("Target","int","0",1),
            "Program"		=>array("Program","int","0",1),
            "Propatie"		=>array("Propatie","text","",1),
            "Description"	=>array("Description","text","",1),
            "etc3"			=>array("etc3","text","",1),
            "etc4"			=>array("etc4","text","",1)
    );

    public function insert($status){

        $status = $this->validate->add($status);

        $sql = "
            INSERT INTO Skill(
                id,Name,Start,Max,MaxLevel,Type,Attribute,MType,Target,Program,Propatie,Description,etc3,etc4
                )
            VALUES(
                :id,:Name,:Start,:Max,:MaxLevel,:Type,:Attribute,:MType,:Target,:Program,:Propatie,:Description,:etc3,:etc4
            )
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Start'		=> $status['Start'],
                    ':Max'			=> $status['Max'],
                    ':MaxLevel'		=> $status['MaxLevel'],
                    ':Type'			=> $status['Type'],
        			':Attribute'	=> $status['Attribute'],
        			':MType'		=> $status['MType'],
                    ':Target'		=> $status['Target'],
                    ':Program'		=> $status['Program'],
                    ':Propatie'		=> $status['Propatie'],
                    ':Description'	=> $status['Description'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
                ));
    }

    public function update($status){

        $status = $this->validate->add($status);

        $sql = "
        UPDATE skill SET
            Name		= :Name,
            Start		= :Start,
            Max			= :Max,
            MaxLevel	= :MaxLevel,
            Type		= :Type,
        	Attribute	= :Attribute,
        	MType		= :MType,
            Target		= :Target,
            Program		= :Program,
            Propatie	= :Propatie,
            Description	= :Description,
            etc3		= :etc3,
            etc4		= :etc4
        WHERE
            id			= :id
        ";

        $stmt = $this->execute($sql,array(
                    ':id'			=> $status['id'],
                    ':Name'			=> $status['Name'],
                    ':Start'		=> $status['Start'],
                    ':Max'			=> $status['Max'],
                    ':MaxLevel'		=> $status['MaxLevel'],
                    ':Type'			=> $status['Type'],
        			':Attribute'	=> $status['Attribute'],
        			':MType'		=> $status['MType'],
                    ':Target'		=> $status['Target'],
                    ':Program'		=> $status['Program'],
                    ':Propatie'		=> $status['Propatie'],
                    ':Description'	=> $status['Description'],
                    ':etc3'			=> $status['etc3'],
                    ':etc4'			=> $status['etc4']
        ));
    }

    public function fetchAllSkill(){
        $sql = "
        SELECT
            id,
            Name,
            Start,
            Max,
            MaxLevel,
            Type,
        	Attribute,
        	MType,
            Target,
            Program,
            Propatie,
            Description,
            etc3,
            etc4
        FROM
            skill

        ";

        return $this->fetchAll($sql,array());
    }


    public function fetchSkillId(){
        $sql = "
        SELECT
            id,
            Name,
            Start,
            Max,
            MaxLevel,
            Type,
        	Attribute,
        	MType,
            Target,
            Program,
            Propatie,
            Description,
            etc3,
            etc4
        FROM
            skill
        WHERE
            id		= :id
        ";

        return $this->fetch($sql,array(
                ':id'			=> $status['id'],
        ));
    }

}

