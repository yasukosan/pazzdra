$(document).ready(function(){
    pazzle.setAction();
});


var pazzle = {

    hoge:""
    ,draged		: false
    ,dragBefor	: ""
    ,dragAfter	: ""
    ,dragTarget	: $("#dObj")
    ,lastTarget	: {}
    ,dragClass	: ""

    ,dropX: 5
    ,dropY: 20

    ,setAction: function(){
        $(document).bind("mousemove",function(e){
            if(pazzle.draged){
                $(pazzle.dragTarget).css({
                    top:(e.clientY - pazzle.dropX),
                    left:(e.clientX - pazzle.dropY)
                });

                pazzle.deselect();
            }
        });

        $(".pazzle > ul").find("li").each(function(){

            $(this).bind("mousedown",function(e){
                pazzle.draged = true;
                pazzle.dragClass = $(this).attr("class");
                pazzle.lastTarget = this;
                $(pazzle.lastTarget).removeClass();

                $(pazzle.dragTarget).show();
                pazzle.dragTarget.addClass(pazzle.dragClass);
                pazzle.dragTarget.css({
                    top:(e.clientY - pazzle.dropX),
                    left:(e.clientX - pazzle.dropY)
                });
            });

            $(this).bind("mouseenter",function(e){
                if(pazzle.draged){
                    $(pazzle.lastTarget).addClass($(this).attr("class"));
                    pazzle.lastTarget = this;
                    $(pazzle.lastTarget).removeClass();
                }
            });
        });

        $(document).bind("mouseup",function(){
            if(pazzle.draged){
                pazzle.reset();
            }
        });
    }

    ,reset: function(){
        pazzle.draged = false;
        $(pazzle.dragTarget).removeClass();
        $(pazzle.dragTarget).hide();
        $(pazzle.lastTarget).addClass(pazzle.dragClass);
    }

    ,deselect: function() {
        if(window.getSelection){
            window.getSelection().removeAllRanges();
        }else if(document.selection){
            selection.setEndPoint("EndToStart", document.selection.createRange());
            selection.select();
        }
    }




    ,pazzleMap:[
               ["","","","","",""],
               ["","","","","",""],
               ["","","","","",""],
               ["","","","","",""],
               ["","","","","",""]]



    ,comboX: {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]}
    ,comboY: {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]}
    ,combo: {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]}

    ,checkDrop	: ""
    ,checkCount	: 0
    ,lineY		: 0
    ,lineX		: 0

    ,countX: function(){

    	var setPazzleMap = function(){
            pazzle.comboX[pazzle.checkDrop].push({
                num		: pazzle.checkCount,
                y		: pazzle.lineY,
                x		: pazzle.lineX
            });
    	}

        for(var i = 0; i <= 4; i++){

            pazzle.checkDrop = "";
            pazzle.checkCount = 0;

            for(var j = 0; j <= 5; j++){

                if(pazzle.checkCount > 0){
                    if(pazzle.pazzleMap[i][j] == pazzle.checkDrop){

                        pazzle.checkCount++;

                        if(pazzle.checkCount == 6){

                        	setPazzleMap();
                            pazzle.setDropCount(1,j,i);

                        }else if(pazzle.checkCount >= 3 && j == 5){

                        	setPazzleMap();
                        }

                    }else{
                        if(pazzle.checkCount >= 3){

                        	setPazzleMap();

                            pazzle.setDropCount(1,j,i);
                        }else{
                            pazzle.setDropCount(1,j,i);
                        }
                    }
                }else{
                    pazzle.setDropCount(1,j,i);
                }
            }
        }
    }

    ,countY: function(){
        for(var j = 0; j <= 5; j++){

            pazzle.checkDrop = "";
            pazzle.checkCount = 0;

            for(var i = 0; i <= 4; i++){

                if(pazzle.checkCount > 0){

                    if(pazzle.pazzleMap[i][j] == pazzle.checkDrop){

                        pazzle.checkCount++;

                        if(pazzle.checkCount == 5){
                            pazzle.comboY[pazzle.checkDrop].push({
                                num		: pazzle.checkCount,
                                y		: pazzle.lineY,
                                x		: pazzle.lineX
                            });
                            pazzle.setDropCount(1,j,i);

                        }else if(pazzle.checkCount >= 3 && i == 4){
                            pazzle.comboY[pazzle.checkDrop].push({
                                num		: pazzle.checkCount,
                                y		: pazzle.lineY,
                                x		: pazzle.lineX
                            });
                        }

                    }else{

                        if(pazzle.checkCount >= 3){
                            pazzle.comboY[pazzle.checkDrop].push({
                                num		: pazzle.checkCount,
                                y		: pazzle.lineY,
                                x		: pazzle.lineX
                            });
                            pazzle.setDropCount(1,j,i);
                        }else{
                            pazzle.setDropCount(1,j,i);
                        }

                    }
                }else{
                    pazzle.setDropCount(1,j,i);
                }
            }
        }
    }

    ,setDropCount: function(count,x,y){
        pazzle.checkCount = count;
        pazzle.checkDrop = pazzle.pazzleMap[y][x];
        pazzle.lineY = y;
        pazzle.lineX = x;
    }

    ,colors : ["red","blue","green","yellow","purple","heart"]
    ,countCombo: function(){

        for(var i in colors){

            if(pazzle.comboX[pazzle.colors[i]].length > 0){

                if(pazzle.comboY[pazzle.colors[i]].length > 0){

                }else{
                    //横軸揃い
                    pazzle.addComboXonly(pazzle.comboX[pazzle.colors[i]]);
                }
            }else{

                if(pazzle.comboY[pazzle.colors[i]].length > 0){
                    //縦軸揃い
                    pazzle.addComboYonly(pazzle.comboY[pazzle.colors[i]]);

                }else{

                }
            }
        }
    }
    ,addComboXonly: function(color){
        for(var d in pazzle.comboX[color]){
            pazzle.combo[color].push({
                num		: pazzle.comboX[color]["num"]
            });
        }
    }
    ,addComboYonly: function(color){
        for(var d in pazzle.comboY[color]){
            pazzle.combo[color].push({
                num		: pazzle.comboY[color]["num"]
            });
        }
    }
    ,addComboXY: function(color){

    }

    ,comboArrayX :[]
    ,comboArrayY :[]

    ,crosArray: []

    ,comboCros: function(color){

        pazzle.colorArrayX = pazzle.comboX[color];
        pazzle.colorArrayY = pazzle.comboY[color];

        var crosArray = [];

        var finish = false;
        var XorY = "x";

        while(finish){

            if(pazzle.colorArrayX.length > 0
                && pazzle.colorArrayY.length > 0){

                if(XorY == "x"){

                    var Xarray = pazzle.colorArrayX,slice(0,1);
                    var search = pazzle.searchCombo();
                    if(!search){

                        pazzle.colorArrayX.unshift(Xarray);

                    }else{

                        if(crosArray.indexof("x"+Xarray["x"]+"y"+Xarray["y"])){
                            crosArray["x"+Xarray["x"]+"y"+XarrayY["y"]].push("x"+Xarray["x"]+"y"+Xarray["y"]);
                        }else{
                            var name = "x"+pazzle.colorArrayY[i]["x"]+"y"+pazzle.colorArrayY[i]["y"];
                            crosArray.push({
                                name:"x"+pazzle.colorArrayY[(i+1)]["x"]+"y"+pazzle.colorArrayY[(i+1)]["y"]
                            })
                        }

                    }

                }else{
                    var Yarray = pazzle.colorArrayY,slice(0,1);

                }


            }else if(pazzle.colorArrayX.length == 0
                    && pazzle.colorArrayY.length > 0){

                for(var i in pazzle.colorArrayY){

                    if(i <= (pazzle.colorArrayY.length - 1)){
                        if(pazzle.arrayComparisonConcurrent(
                                pazzle.colorArrayY[i],
                                pazzle.colorArrayY[(i+1)],
                                "y")){

                            if(crosArray.indexof("x"+pazzle.colorArrayY[i]["x"]+"y"+pazzle.colorArrayY[i]["y"])){
                                crosArray["x"+pazzle.colorArrayY[i]["x"]+"y"+pazzle.colorArrayY[i]["y"]].push("x"+pazzle.colorArrayY[(i+1)]["x"]+"y"+pazzle.colorArrayY[(i+1)]["y"]);
                            }else{
                                var name = "x"+pazzle.colorArrayY[i]["x"]+"y"+pazzle.colorArrayY[i]["y"];
                                crosArray.push({
                                    name:"x"+pazzle.colorArrayY[(i+1)]["x"]+"y"+pazzle.colorArrayY[(i+1)]["y"]
                                })
                            }


                        }else{
                            pazzle.addComboYonly(pazzle.comboY[pazzle.colorArrayY[i]]);
                        }
                    }

                    if(i == pazzle.colorArrayY.lenght){
                        pazzle.addComboYonly(pazzle.comboY[pazzle.colorArrayY[i]]);
                    }
                }


            }else if(pazzle.colorArrayX.length > 0
                    && pazzle.colorArrayY.length == 0){

                for(var i in pazzle.colorArrayX){

                    if(i <= (pazzle.colorArrayX.length - 1)){
                        if(pazzle.arrayComparisonConcurrent(
                                pazzle.colorArrayX[i],
                                pazzle.colorArrayX[(i+1)],
                                "x")){

                            if(crosArray.indexof("x"+pazzle.colorArrayX[i]["x"]+"y"+pazzle.colorArrayX[i]["y"])){
                                crosArray["x"+pazzle.colorArrayX[i]["x"]+"y"+pazzle.colorArrayX[i]["y"]].push("x"+pazzle.colorArrayY[(i+1)]["x"]+"y"+pazzle.colorArrayY[(i+1)]["y"]);
                            }else{
                                var name = "x"+pazzle.colorArrayX[i]["x"]+"y"+pazzle.colorArrayX[i]["y"];
                                crosArray.push({
                                    name:"x"+pazzle.colorArrayX[(i+1)]["x"]+"y"+pazzle.colorArrayX[(i+1)]["y"]
                                })
                            }


                        }else{
                            pazzle.addComboXonly(pazzle.comboX[pazzle.colorArrayX[i]]);
                        }
                    }

                    if(i == pazzle.colorArrayY.lenght){
                        pazzle.addComboYonly(pazzle.comboY[pazzle.colorArrayY[i]]);
                    }

                }

            }else{
                finish = true;
            }
        }


    }

    //パズルが重なっているか調べる
    ,searchCombo: function(sArray,nArray){

        var cros = []

        for(var i in sArray){
            if(i > 0){
                for(var j = 0; j<= (nArray.length - 1); j++){
                    if(pazzle.arrayComparisonCros(sArray[i],nArray[j])){
                        cros.push("x"+nArray[j]["x"]+"y"+nArray[j]["y"]);
                    }
                }
            }
        }

        if(cros.length == 0){
            return false;
        }else{
            return cros;
        }
    }


    ,getMap: function(){

        pazzle.mapReset();

        for(var i = 0; i <= 5; i++){
            $(".pazzle > ul").eq(i).find("li").each(function(l){
                pazzle.pazzleMap[i][l] = $(this).attr("class");
            });
        }

        pazzle.countX();
        pazzle.countY();
    }

    ,mapReset: function(){
        pazzle.checkCount = 0;
        pazzle.comboX = {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]};
        pazzle.comboY = {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]};
        pazzle.pazzleMap = [
                    ["","","","","",""],
                    ["","","","","",""],
                    ["","","","","",""],
                    ["","","","","",""],
                    ["","","","","",""]];
    }


    //ドロップ毎の座標のかぶりを検索
    ,arrayComparisonCros: function(a1,a2){
        for(var i = 0; i <= (a1.length - 1); i++){
            for(var j = 0; j<= (a2.length -1); j++){
                if(a1[i]["x"] == a2[j]["x"]
                    && a1[i]["y"] == a2[j]["y"]){
                    return true;
                }
            }
        }
        return false;
    }

    ,arrayComparisonConcurrent: function(a1,a2,line){

        for(var i = 0; i <= (a1.length - 1); i++){
            for(var j = 0; j<= (a2.length -1); j++){
                if(line == "x"){
                    if((a1[i]["x"] + 1) == a2[j]["x"]
                    && a1[i]["y"] == a2[j]["y"]){
                            return true;
                        }

                }else if(line =="y"){
                    if(a1[i]["x"] == a2[j]["x"]
                    && (a1[i]["y"] + 1 )== a2[j]["y"]){
                            return true;
                        }
                }
            }
        }
        return false;
    }
}