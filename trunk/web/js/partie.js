
var partie = {

    hoge:""
    ,PartieList:{}
    ,PartieListOld:{}


    ,setMonster:function(num){
        this.resetMonster(num);

        data.PartieResult[0][num] = $.extend(true,{},data.PartieList[0][num]);

        var tag = "div.monster_"+num+" div.monster-";
        var mons = data.PartieList[0][num];
        var result = data.PartieResult[0][num];
        var Skill = skill.skills[mons["Skill"]];
        var Leader = leader.leaderSkills[mons["Reader"]];

        var ar = monster.arousalList(mons["Arousal"]);

        //ステータス計算
        this.setStatus(num,tag);


        partie.PartieListOld[num] = data.PartieList[0][num];
        partie.setLeaderStatus(num);

        arousal.setArousal(mons["Arousal"],num);


        //モンスターのステータス表示
        this.setView(num,Skill,Leader,ar,tag);

        this.setTotal();

        //覚醒数の合計を取得
        arousal.getArousals();
    }

    ,setPlusStatus: function(num){
        var tag = "div.monster_"+num+" div.monster-";
        var mons = data.PartieList[0][num];
        var re = data.PartieResult[0][num];

        $(tag+"hp input").val(0);
        $(tag+"atack input").val(0);
        $(tag+"atack-sub input").val(0);
        $(tag+"cure input").val(0);

        //ステータス計算
        this.setStatus(num,tag);

        return function(){
            //モンスターのステータス表示
            $(tag+"hp input").val(re["HP"]);
            $(tag+"atack input").val(re["Attack"]);
            $(tag+"atack-sub input").val(re["SubAttack"]);
            $(tag+"cure input").val(re["Cure"]);
        }();
    }
    ,setView:function(num,skill,leader,ar,tag){

        var re = data.PartieResult[0][num];
        var mons = data.PartieList[0][num];

        $(tag+"number").append(re["id"]);
        //$(tag+"type1").append("<p class='t"+re["Type"]+"'></p><p>"+data.Types[re["Type"]]+"</p>");
        //$(tag+"type2").append("<p class='t"+re["SubType"]+"'></p><p>"+data.Types[re["SubType"]]+"</p>");
        $(tag+"type1").append(data.Types[re["Type"]]);
        $(tag+"type2").append(data.Types[re["SubType"]]);
        $(tag+"name").append("<p>"+re["Name"]+"</p>");
        $(tag+"img").append("<img src='img/m/"+mons["id"]+".jpg'>");
        $(tag+"hp input").val(re["HP"]);
        $(tag+"atack input").val(re["Attack"]);
        $(tag+"atack-sub input").val(re["SubAttack"]);
        $(tag+"cure input").val(re["Cure"]);
        $(tag+"skill").append("<p>"+skill["Name"]+"</p><p>"+skill["Description"]+"</p>");
        $(tag+"reader").append("<p>"+leader["Name"]+"</p><p>"+leader["Description"]+"</p>");
        $(tag+"arousal").append(ar);

        if(num == 0){
            $("#party-t-leader1").empty()
            $("#party-t-leader1").append("<p>"+leader["Name"]+"</p>");
            leader.leaderSkill = leader["id"];
        }else if(num == 5){
            $("#party-t-leader2").empty()
            $("#party-t-leader2").append("<p>"+leader["Name"]+"</p>");
            leader.subLeaderSkill = leader["id"];
        }
    }

    /*
     * ステータスの計算
     * プラス値の計算、副攻撃、攻撃覚醒等の計算
     */

    ,setStatus: function(num,tag){
        var mons = data.PartieList[0][num];
        var result = data.PartieResult[0][num];

        var ar = arousal.getBStatusUp(result["Arousal"]);

        var at = Number(mons["Attack"]) + this.getPlus(tag,"atack",num) + ar[1]

        result["Attack"] = at;

        if(mons["SubAttribute"] != 0 && mons["SubAttribute"] != undefined){
            if(mons["Attribute"] == mons["SubAttribute"]){
                result["SubAttack"] = Math.ceil(at * 0.1);
            }else{
                result["SubAttack"] = Math.ceil(at * 0.3);
            }

        }else{
            result["SubAttack"] = 0;
        }

        result["HP"] = Number(mons["HP"]) + this.getPlus(tag,"hp",num)  + ar[0];
        result["Cure"] = Number(mons["Cure"]) + this.getPlus(tag,"cure",num) + ar[2];
    }

    ,setHP: function(){

    }
    ,setCure: function(){

    }

    ,setCalcMonster:function(){

        this.resetCalcMonster();

        var aAttack = 0;
        var aSubAttack = 0;
        var aColor = {0:0,1:0,2:0,3:0,4:0,5:0}
        var aSubColor = {0:0,1:0,2:0,3:0,4:0,5:0}
        var aHP = 0;
        var aCure = 0;

        for(var i in this.PartieList){
            var tag = "div.pfs_m_"+i+" div.monster-";
            var mons = data.PartieList[0][i];
            var result = data.PartieResult[0][i];
            var st = data.FinalStatus[i];

            $(tag+"name").append("<p>"+mons["Name"]+"</p>");
            $(tag+"img").append("<div class='m"+mons["id"]+"'></div>");
            $(tag+"hp").append("<p>ＨＰ</p><p>"+st[4]+"</p>");
            $(tag+"atack").append("<p>攻主</p><p>"+st[2]+"</p>");
            $(tag+"atack-sub").append("<p>攻副</p><p>"+st[3]+"</p>");
            $(tag+"cure").append("<p>回復</p><p>"+st[5]+"</p>");

            aAttack 	+= st[2];
            aSubAttack 	+= st[3];
            aHP   		+= st[4];
            aCure 		+= st[5];

            aColor[mons["Attribute"]] += st[2];
            aSubColor[mons["SubAttribute"]] += st[3];

        }

        var t = "p#pfs-";
        $(t+"hp").append(aHP);
        $(t+"attack").append(aAttack);
        $(t+"attack-sub").append(aSubAttack);
        $(t+"cure").append(aCure);
        $(t+"color1").append(aColor[1]);
        $(t+"color2").append(aColor[2]);
        $(t+"color3").append(aColor[3]);
        $(t+"color4").append(aColor[4]);
        $(t+"color5").append(aColor[5]);
        $(t+"color1-sub").append(aSubColor[1]);
        $(t+"color2-sub").append(aSubColor[2]);
        $(t+"color3-sub").append(aSubColor[3]);
        $(t+"color4-sub").append(aSubColor[4]);
        $(t+"color5-sub").append(aSubColor[5]);
    }

    ,resetCalcMonster:function(){
        for(var i = 0; i <= 6; i++){
            var tag = "div.pfs_m_"+i+" div.monster-";

            $(tag+"name").empty();
            $(tag+"img").empty();
            $(tag+"hp").empty();
            $(tag+"atack").empty();
            $(tag+"atack-sub").empty();
            $(tag+"cure").empty();
        }
        var t = "p#pfs-";
        $(t+"hp").empty();
        $(t+"attack").empty();
        $(t+"attack-sub").empty();
        $(t+"cure").empty();
        $(t+"color1").empty();
        $(t+"color2").empty();
        $(t+"color3").empty();
        $(t+"color4").empty();
        $(t+"color5").empty();
        $(t+"color1-sub").empty();
        $(t+"color2-sub").empty();
        $(t+"color3-sub").empty();
        $(t+"color4-sub").empty();
        $(t+"color5-sub").empty();
    }


    ,plusList:[[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]]

    ,getPlus:function(tag,target,n){

        var num = Number($(tag+target+" p").eq(3).html());

        if(num > 0){

            var pl = data.PlusList[0][n];

            if(target == "hp"){
                pl[0] = num;
                return num * 10;
            }else if(target == "atack"){
                pl[1] = num;
                return num * 5;
            }else if(target == "cure"){
                pl[2] = num;
                return num * 3;
            }
        }else{
            return 0;
        }
    }
    ,resetPlus:function(target,n){

        var pl = data.PlusList[0][n];

        if(target == "hp"){
            pl[0] = 0;
            return pl[0] * 10;
        }else if(target == "atack"){
            pl[1] = 0;
            return pl[1] * 5;
        }else if(target == "cure"){
            pl[2] = 0;
            return pl[2] * 3;
        }
    }

    ,resetMonster:function(num){
        var tag = "div.monster_"+num+" div.monster-";

        if($(tag+" div.monster-name").html() != ""){
            var pp = partie.PartieListOld[num];

            var at = Number($("#party-t-attack").html());
            $("#party-t-attack").empty().append(0);
            var hp = Number($("#party-t-hp").html());
            $("#party-t-hp").empty().append(0);
            var cr = Number($("#party-t-cure").html());
            $("#party-t-cure").empty().append(0);
            var co = Number($("#party-t-cost").html());
            $("#party-t-cost").empty().append(0);
            var re = Number($("#party-t-rea").html());
            $("#party-t-rea").empty().append(0);
        }
        $(tag+"number").empty();
        $(tag+"type1").empty();
        $(tag+"type2").empty();
        $(tag+"name").empty();
        $(tag+"img").empty();
        $(tag+"hp input").val(0);
        $(tag+"atack input").val(0);
        $(tag+"atack-sub input").val(0);
        $(tag+"cure input").val(0);
        $(tag+"skill").empty();
        $(tag+"reader").empty();
        $(tag+"arousal").empty();
    }

    ,setTotal: function(){
        var at = 0;var sat = 0;var hp = 0;var cure = 0;var cost = 0;var rea = 0;
        var pr = data.PartieResult[0];

        for(var i in pr){
            at = (pr[i]["Attack"] != undefined)?Number(pr[i]["Attack"])+at:at;
            sat = (pr[i]["SubAttack"] != undefined)?Number(pr[i]["SubAttack"])+sat:sat;
            hp = (pr[i]["HP"] != undefined)?Number(pr[i]["HP"])+hp:hp;
            cure = (pr[i]["Cure"] != undefined)?Number(pr[i]["Cure"])+cure:cure;
            cost = (pr[i]["Cost"] != undefined)?Number(pr[i]["Cost"])+cost:cost;
            rea = (pr[i]["Rarity"] != undefined)?Number(pr[i]["Rarity"])+rea:rea;
        }

        $("#party-t-attack").empty().append(at);
        $("#party-t-subattack").empty().append(sat);
        $("#party-t-hp").empty().append(hp);
        $("#party-t-cure").empty().append(cure);
        $("#party-t-cost").empty().append(cost);
        $("#party-t-rea").empty().append(rea);
    }



    ,setLeaderStatus: function(num){
        data.LeaderResult[0][num] = $.extend(true,{},data.PartieList[0][num]);
    }

}