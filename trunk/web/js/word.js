
var word = {

    hoge:""

    ,setuped: false
    ,setup: function(){
        if(!this.setuped){
            this.setFilterEvent();
            this.setuped = true;
        }
    }

    ,showMonsterList: function(){

        $("#monster-box2").empty();

        for(var i in monster.monsters){

            if(i > 0){

                var ss = skill.skills[i];

                if(this.dmapFilter[i] == undefined){
                    this.dmapFilter[i] = 0;
                }

                if(this.dmapFilter[i] < 1){
                    $("ul#monster-box2").append("" +
                            "<li data-role='"+m["id"]+"'>" +
                            //"<div class='ml"+m["id"]+"' alt='"+m["Name"]+"'></div>" +
                            "<div class='mimage-l'><img src='img/m/"+m["id"]+".jpg' /></div>" +
                            "<div >"+m["id"]+"</div>" +
                            "<div >"+m["Name"]+"</div>" +
                            "</li>");
                }
            }
        }

        return function(){
            skilltype.setViewEvent();
        }();
    }

    ,monsFilter:[]
    ,searched: true
    ,setFilterEvent: function(){
        $("#word_search").bind("keyup",function(){
            if(word.searched){
                word.searched = false;
                setTimeout(word.buildMonsterList, 500);
            }
        });
    }

    ,buildMonsterList: function(){
        $("#monster-box2").empty();
        String.prototype.toOneNumeric = function(){
            return this.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
                return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
            });
        }
        var w = $("#word_search").val();
        var wcheck = w.toOneNumeric();

        if(isFinite(wcheck) && $("#word_search").val() != ""){

            for(var i in monster.nameMap){

                var num = monster.monsters[i]["id"].indexOf(w)

                if(num >= 0){
                    var m = monster.monsters[i];
                    $("ul#monster-box2").append("" +
                            "<li data-role='"+m["id"]+"'>" +
                            //"<div class='ml"+m["id"]+"' alt='"+m["Name"]+"'></div>" +
                            "<div class='mimage-l'><img src='img/m/"+m["id"]+".jpg' /></div>" +
                            "<div >"+m["id"]+"</div>" +
                            "<div >"+m["Name"]+"</div>" +
                            "</li>");
                }
            }

            return function(){
                word.setViewEvent();
            }();
        }else if($("#word_search").val() != ""){

            for(var i in monster.nameMap){

                var num = monster.nameMap[i].indexOf(w)

                if(num >= 0){
                    var m = monster.monsters[i];
                    $("ul#monster-box2").append("" +
                            "<li data-role='"+m["id"]+"'>" +
                            //"<div class='ml"+m["id"]+"' alt='"+m["Name"]+"'></div>" +
                            "<div class='mimage-l'><img src='img/m/"+m["id"]+".jpg' /></div>" +
                            "<div >"+m["id"]+"</div>" +
                            "<div >"+m["Name"]+"</div>" +
                            "</li>");
                }
            }

            return function(){
                word.setViewEvent();
            }();
        }else{
            word.searched = true;
        }
    }

    ,clear:function(){
        $("#word_search").val("");
    }
    ,setViewEvent: function(){
        $("ul#monster-box2 > li").each(function(){
            $(this).bind("click",function(){
                monster.buildView(this);
            })
        })

        return function(){

            word.searched = true;
        }();
    }

}
