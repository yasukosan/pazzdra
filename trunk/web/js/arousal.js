var arousal = {

    hoge:   ""
    , arousals:  false
    , getArousalData:function(){
        AjaxLoader.load(
                "job=geta&id=all",
                'aj',
                "",
                function(target, msg){
                    arousal.arousals = eval(msg);
                }
            );
    }

    , arousalList: [0,0,0,0,0,0]
    , setArousal: function(arousal, n) {
        if (this.arousalList[n].length > 0) {
            this.resetArousal(n);
        }

        if (arousal.length > 0) {
            var ar = arousal.split(",");

            for (var i in ar){
                var num = Number($("p#pa"+ar[i]).html());
                $("#pa"+ar[i]).html((num + 1));
            }

            this.arousalList[n] = arousal;
        }
    }

    ,resetArousal: function(n){
        var ar = this.arousalList[n].split(",");

        for(var i in ar){
            var num = Number($("#pa"+ar[i]).html());
            $("#pa"+ar[i]).html((num - 1));
        }
        this.arousalList[n] = [];
    }

    ,getArousals: function(){
        for(var i= 1; i <= 29; i++){
            var num = Number($("#pa"+i).html());
            if(num > 0){
                data.ArousalList[0][i] = num;
            }
        }
    }

    ,getBStatusUp:function(ar){
        var hp = 0;
        var at = 0;
        var cu = 0;
        var arl = ar.split(",");

        for(var i in arl){
            if(1 == Number(arl[i])){
                hp++;
            }else if(2 == Number(arl[i])){
                at++;
            }else if(3 == Number(arl[i])){
                cu++;
            }
        }
        var st = [0,0,0];

        if(hp > 0){
            st[0] = 200 * hp;
        }
        if(at > 0){
            st[1] = 100 * at;
        }
        if(cu > 0){
            st[2] = 50 * cu;
        }

        return st;
    }
}