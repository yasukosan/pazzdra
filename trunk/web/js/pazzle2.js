$(document).ready(function(){
    pazzle.setAction();
    edit.setSave();
});


var pazzle = {

    hoge:""
    ,draged		: false
    ,dragBefor	: ""
    ,dragAfter	: ""
    ,dragTarget	: $("#dObj")
    ,lastTarget	: {}
    ,dragClass	: ""

    ,dropX: 5
    ,dropY: 20

    ,comboCount: 0
    ,colorMap: []

    ,setAction: function(){
        $(document).bind("mousemove",function(e){
            if(pazzle.draged){
                $(pazzle.dragTarget).css({
                    top:(e.pageY - pazzle.dropX - 100),
                    left:(e.pageX - pazzle.dropY - 100)
                });

                pazzle.deselect();
            }
        });

        $(".pazzle > ul").find("li").each(function(){

            $(this).bind("mousedown",function(e){
                pazzle.draged = true;
                pazzle.dragClass = $(this).attr("class");
                pazzle.lastTarget = this;
                $(pazzle.lastTarget).removeClass();

                $(pazzle.dragTarget).show();
                pazzle.dragTarget.addClass(pazzle.dragClass);
                pazzle.dragTarget.css({
                    top:(e.pageY - pazzle.dropX - 100),
                    left:(e.pageX - pazzle.dropY - 100)
                });
            });

            $(this).bind("mouseenter",function(e){
                if(pazzle.draged){
                    $(pazzle.lastTarget).addClass($(this).attr("class"));
                    pazzle.lastTarget = this;
                    $(pazzle.lastTarget).removeClass();

                    $(pazzle.dragTarget).css({
                        top:(e.pageY - pazzle.dropX - 100),
                        left:(e.pageX - pazzle.dropY - 100)
                    });
                }
            });
        });

        $(document).bind("mouseup",function(){
            if(pazzle.draged){
                pazzle.reset();
            }
        });
    }

    ,delAction: function(){
        $(document).unbind("mousemove");
        $(document).bind("mouseup");
        $(".pazzle > ul").find("li").each(function(){
            $(this).unbind("mousedown");
            $(this).unbind("mouseenter");
        });
    }
    ,reset: function(){
        pazzle.draged = false;
        $(pazzle.dragTarget).removeClass();
        $(pazzle.dragTarget).hide();
        $(pazzle.lastTarget).addClass(pazzle.dragClass);
    }

    ,deselect: function() {
        if(window.getSelection){
            window.getSelection().removeAllRanges();
        }else if(document.selection){
            selection.setEndPoint("EndToStart", document.selection.createRange());
            selection.select();
        }
    }




    ,pazzleMap:[]
    ,pazzleMapComv:[]

    ,dropColors: ["red","blue","green","yellow","purple","heart"]

    ,colorDropMap: {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]}
    ,colorDropTag: {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]}

    /**
     *
     *  pazzle.combo
     *  各色毎に登録されるパラメーター
     *  [消した数,2way,列強化,強化ドロップ]
     *
     */
    ,combo: {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]}
    ,lineCounter: {red:[0],blue:[0],green:[0],yellow:[0],purple:[0],heart:[0]}
    ,fourCounter: {red:[0],blue:[0],green:[0],yellow:[0],purple:[0],heart:[0]}

    ,checkDrop	: ""
    ,checkCount	: 0
    ,lineY		: 0
    ,lineX		: 0


    ,countX: function(){
        var checkDrop = "";
        var checkCount = 0;
        var lineX = 0;
        var lineY = 0;
        var setPazzleMap = function(){
            for(var i = 0; i <= checkCount - 1; i++){
                pazzle.pazzleMapComv[lineY][(lineX + i)] = checkDrop;
            }
        }
        var setDropCount = function(count,x,y){
            checkCount = count;
            checkDrop = pazzle.pazzleMap[y][x];
            lineY = y;
            lineX = x;
        }

        for(var i = 0; i <= 4; i++){

            checkDrop = "";
            checkCount = 0;
            lineX = 0;
            lineY = 0;

            for(var j = 0; j <= 5; j++){
                if(checkCount > 0){
                    if(pazzle.pazzleMap[i][j].match(checkDrop) != null){
                        checkCount++;

                        if(checkCount == 6){
                            setPazzleMap();
                            setDropCount(1,j,i);
                        }else if(checkCount >= 3 && j == 5){
                            setPazzleMap();
                        }
                    }else{
                        if(checkCount >= 3){
                            setPazzleMap();
                            setDropCount(1,j,i);
                        }else{
                            setDropCount(1,j,i);
                        }
                    }
                }else{
                    setDropCount(1,j,i);
                }
            }
        }
    }

    ,countY: function(){

        var checkDrop = "";
        var checkCount = 0;
        var lineX = 0;
        var lineY = 0;
        var setPazzleMap = function(){
            for(var i = 0; i <= checkCount - 1; i++){
                pazzle.pazzleMapComv[(lineY + i)][lineX] = checkDrop;
            }
        }
        var setDropCount = function(count,x,y){
            checkCount = count;
            checkDrop = pazzle.pazzleMap[y][x];
            lineY = y;
            lineX = x;
        }

        for(var j = 0; j <= 5; j++){
            checkDrop = "";
            checkCount = 0;
            lineX = 0;
            lineY = 0;

            for(var i = 0; i <= 4; i++){
                if(checkCount > 0){
                    if(pazzle.pazzleMap[i][j].match(checkDrop) != null){
                        checkCount++;
                        if(checkCount == 5){
                            setPazzleMap();
                            setDropCount(1,j,i);
                        }else if(checkCount >= 3 && i == 4){
                            setPazzleMap();
                        }
                    }else{
                        if(checkCount >= 3){
                            setPazzleMap();
                            setDropCount(1,j,i);
                        }else{
                            setDropCount(1,j,i);
                        }
                    }
                }else{
                    setDropCount(1,j,i);
                }
            }
        }
    }

    ,getMap: function(){

        pazzle.mapReset();

        for(var i = 0; i <= 5; i++){
            $(".pazzle > ul").eq(i).find("li").each(function(l){
                pazzle.pazzleMap[i][l] = $(this).attr("class");
            });
        }

        return function(){
            pazzle.countX();
            pazzle.countY();

            return function(){
                for(var i = 0; i <= 5; i++){
                    for(var j = 0; j <= 4; j++){
                        if(pazzle.pazzleMapComv[j][i] != "" && pazzle.pazzleMapComv[j][i] != undefined){
                            var colorName = "x"+i+"y"+j;
                            var color = pazzle.pazzleMapComv[j][i];

                            var strong = 0;

                            if(color.match("plus") != null){
                                strong = 1;
                                color = color.replace(/_plus/g, "");
                            }
                            pazzle.colorDropMap[color][colorName] = [j,i,strong];
                            pazzle.colorDropTag[color].push(colorName);
                        }else{
                            pazzle.pazzleMapComv[j][i] = "none";
                        }
                    }
                }

                return function(){
                    for(var i in pazzle.dropColors){
                        pazzle.comboConter(pazzle.dropColors[i]);
                    }

                    return function(){
                        pazzle.showCombo();
                    }()
                }();
            }();
        }();
    }

    ,showCombo: function(){

        $("ul.combo-list-view").empty();
        var counter = 1;

        for(var i in pazzle.combo){

            for(var j in pazzle.combo[i]){
                if(pazzle.combo[i][j][1] == 1){
                    $("ul.combo-list-view").append(
                            "<li>" +
                            "<p class='combo-c'>"+counter+"</p>" +
                            "<p class='combo-d'>"+i+"　"+pazzle.combo[i][j][0]+"</p>" +
                            "<p class='combo-2'>2WAY</p>" +
                            "<p class='combo-s'>強化"+pazzle.combo[i][j][3]+"</p>" +
                            "</li>")
                }else if(pazzle.combo[i][j][2] == 1){
                    $("ul.combo-list-view").append(
                            "<li>" +
                            "<p class='combo-c'>"+counter+"</p>" +
                            "<p class='combo-d'>"+i+"　"+pazzle.combo[i][j][0]+"</p>" +
                            "<p class='combo-l'>列強化</p>" +
                            "<p class='combo-s'>強化"+pazzle.combo[i][j][3]+"</p>" +
                            "</li>");
                }else{
                    $("ul.combo-list-view").append(
                            "<li>" +
                            "<p class='combo-c'>"+counter+"</p>" +
                            "<p class='combo-d'>"+i+"　"+pazzle.combo[i][j][0]+"</p>" +
                            "<p class='combo-s'>強化"+pazzle.combo[i][j][3]+"</p>" +
                            "</li>");
                }
                counter++;
            }

            var dc = this.dropColors.indexOf(i) + 1;
            if(this.colorMap.indexOf(dc) < 0){
                this.colorMap.push(dc);
            }

        }

        this.comboCount = counter - 1;
    }

    ,setDropMap: function(color,x,y){
        var colorName = "x"+x+"y"+y;
        pazzle.colorDropMap[color][colorName] = [x,y];

    }


    ,mapReset: function(){
        pazzle.colorDropMap = {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]};
        pazzle.colorDropTag = {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]};

        pazzle.combo = {red:[],blue:[],green:[],yellow:[],purple:[],heart:[]};
        pazzle.lineCounter = {red:[0],blue:[0],green:[0],yellow:[0],purple:[0],heart:[0]};
        pazzle.fourCounter = {red:[0],blue:[0],green:[0],yellow:[0],purple:[0],heart:[0]};
        pazzle.pazzleMap = [
                    ["","","","","",""],
                    ["","","","","",""],
                    ["","","","","",""],
                    ["","","","","",""],
                    ["","","","","",""]];
        pazzle.pazzleMapComv = [
                            ["","","","","",""],
                            ["","","","","",""],
                            ["","","","","",""],
                            ["","","","","",""],
                            ["","","","","",""]];
    }

    ,comboConter: function(color){

        var colorArray = pazzle.colorDropTag[color];

        var searchCounter = true;
        var searchArray = [];
        var dropCounter = 1;

        var strongCounter = 0;

        var fourFlag = false;

        var lineFlag = [0,0,0,0,0];
        var lineCounter = [0,0,0,0,0];

        var lineDeleteCheck = function(x,y){
            if(x == 0){
                lineFlag[y] = 1;
                lineCounter[y]=1;
            }else if(lineFlag[y]){
                lineCounter[y]++;
            }
        };


        while(colorArray.length > 0){

            if(searchCounter){

                searchArray.push([colorArray.shift()]);
                searchArray[0].push(4);
                searchCounter = false;

            }

            while(searchArray.length > 0){

                var targetName =  searchArray.shift()
                var searchTarget = pazzle.colorDropMap[color][targetName[0]];

                if(searchTarget[2]){
                    strongCounter = strongCounter + 1;
                }

                for(var i = 0; i <= 3; i++){
                    if(targetName[1] != i){
                        if(i == 0){
                            var searchX = searchTarget[1];
                            var searchY = searchTarget[0] - 1;
                            var vector = 2;
                        }else if(i == 1){
                            var searchX = searchTarget[1] + 1;
                            var searchY = searchTarget[0];
                            var vector = 3;
                        }else if(i == 2){
                            var searchX = searchTarget[1];
                            var searchY = searchTarget[0] + 1;
                            var vector = 0;
                        }else if(i == 3){
                            var searchX = searchTarget[1] - 1;
                            var searchY = searchTarget[0];
                            var vector = 1;
                        }
                    }

                    if(colorArray.indexOf("x"+searchX+"y"+searchY) >= 0){
                        searchArray.push(["x"+searchX+"y"+searchY,vector]);

                        for(var j = 0; j <= colorArray.length; j++){
                            if(colorArray[j] == "x"+searchX+"y"+searchY){
                                colorArray.splice(j,1);

                            }
                        }
                        dropCounter++;
                        lineDeleteCheck(searchTarget[1],searchTarget[0]);
                    }
                }
            }


            var fourCheck = false;
            var lineCheck = false;

            if(dropCounter == 4){
                fourFlag = true;
                if(pazzle.fourCounter[color] == 0){
                    pazzle.fourCounter[color] = 1;
                    fourCheck = true;
                }else{
                    pazzle.fourCounter[color]++;
                }
            }

            if($.inArray(6, lineCounter) >= 0){
                if(pazzle.lineCounter[color][0] == 0){
                    pazzle.lineCounter[color][0] = 1;
                    lineCheck = true;
                }else{
                    pazzle.lineCounter[color][0]++;
                    lineCheck = true;
                }
            }

            if(dropCounter >= 3){
                if(fourFlag){
                    var four = 1;
                }else{
                    var four = 0;
                }
                if(lineCheck){
                    var line = 1;
                }else{
                    var line = 0;
                }

                pazzle.combo[color].push([dropCounter,four,line,strongCounter]);
            }

            dropCounter = 1;
            searchCounter = true;
            searchArray = [];
            fourFlag = false;
            lineFlag = [0,0,0,0,0];
            lineCounter = [0,0,0,0,0];
            strongCounter = 0;

        }
    }


}