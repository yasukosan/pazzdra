
var evolution = {

    hoge:""

    ,getEvolutionData:function(){
        AjaxLoader.load(
            "job=evol&id=all",
            'aj',
            "",
            function(target,msg){
                data.EvolMap = eval(msg);
            }
        );
    }

	,showed: false
	,show: function(){
		if(this.showed){
			$("#monster-evol").hide();
			$("#monster-evol").empty();
			this.showed = false;
		}else{
			$("#monster-evol").empty();
			$("#monster-evol").show();
			this.showed = true;

			this.buildEvoView();
		}
	}

	,showSwitch: function(){
		if(monster.selectedMonster["Evolution"] > 0){
			$("#monster-ev").show();
		}else{
			$("#monster-ev").hide();
		}
	}

	,buildEvoView: function(){

		var evomap = data.EvolMap[Number(monster.selectedMonster["Evolution"])];

		var evType = evomap["tmp"].split("-");
		var posi = this.positions[evType[0]];
		var count = 1;
		var ulmapp = (posi[(posi.length - 1)] != 0)?posi[(posi.length - 1)][evType[1]]:false;
		var ulmap = (!ulmapp)?false:ulmapp.split(":");

		this.setEvoba(monster.monsters[evomap["eb"] - 1],"b","");
		$('.Ev-b').css({top:posi[1][0]+"px",left:posi[1][1]+"px"});

		for(var i = 1; i <= 6; i++){

			if(evomap["eam"+i] != undefined && evomap["eam"+i] != 0){


				var material = evomap["em"+i].split(":");

				if(i == 1){
					var b = evomap["eb"] - 1;
					var a = evomap["eam"+i] - 1;
				}else{
					var b = evomap["eam"+ (i - 1)] - 1;
					var a = evomap["eam"+i] - 1;
				}

				$("#TmpEvoarr").tmpl({
					id:i,
					num:posi[0][(i-1)]
				}).appendTo("#monster-evol");

				this.setEvop(material,monster.monsters[a],monster.monsters[b],i);
				this.setEvoba(monster.monsters[a],"a",i);


				$('.Ev-ar'+i).css({top:posi[(i+1)][0][0]+"px",left:posi[(i+1)][0][1]+"px"});
				$('.Ev-p'+i).css({top:posi[(i+1)][1][0]+"px",left:posi[(i+1)][1][1]+"px"});
				$('.Ev-a-'+i).css({top:posi[(i+1)][2][0]+"px",left:posi[(i+1)][2][1]+"px"});

				if(ulmap != false && $.inArray(String(i), ulmap) >= 0){
					$('.Ev-a-'+i+" div.ev-ultimate").show();
				}
			}
		}
	}

	,setEvoba:function(mons,tmp,i){
		$("#TmpEvo"+tmp).tmpl({
			posi:i,
			id:mons["id"],
			name:mons["Name"],
			attri:data.colorMap[mons["Attribute"]],
			subattri:data.colorMap[mons["SubAttribute"]],
			typ:mons["Type"],
			subtyp:mons["SubType"],
			rea:mons["Rarity"],
			cost:mons["Cost"],
			HP:mons["HP"],
			At:mons["Attack"],
			Cu:mons["Cure"],
			skil:skill.skills[mons["Skill"]]["Name"],
			skilp:skill.skills[mons["Skill"]]["Description"],
			lea:leader.leaderSkills[mons["Reader"]]["Name"],
			leap:leader.leaderSkills[mons["Reader"]]["Description"]
		}).appendTo("#monster-evol");
	}

	,setEvop:function(mat,a,b,i){

		var att = this.checkChenge(b,a,"at","Attribute");
		var subatt = this.checkChenge(b,a,"at","SubAttribute")
		var ty = this.checkChenge(b,a,"ty","Type");
		var subty = this.checkChenge(b,a,"ty","SubType");
		var hp = this.checkStUP((a["HP"] - b["HP"]));
		var at = this.checkStUP((a["Attack"] - b["Attack"]));
		var cu = this.checkStUP((a["Cure"] - b["Cure"]));
		var sk = this.checkChenge(b,a,"sk","Skill");
		var le = this.checkChenge(b,a,"sk","Reader");

		$("#TmpEvop").tmpl({
			posi:i,
			ma1:mat[0],ma2:mat[1],ma3:mat[2],ma4:mat[3],ma5:mat[4],
			atri:att[0],		atrip:att[1],
			subatri:subatt[0],	subatrip:subatt[1],
			type:ty[0],			typep:ty[1],
			subtype:subty[0],	subtypep:subty[1],
			hp:hp[0],			hpp:hp[1],
			at:at[0],			atp:at[1],
			cu:cu[0],			cup:cu[1],
			skilp:sk[1],		lsp:le[1],
		}).appendTo("#monster-evol");
	}

	,checkStUP:function(n){
		if(n == 0){
			return [0,""];
		}else if(n > 0){

			return [n,'ev-up'];
		}else{

			return [n,'ev-down'];
		}
	}

	,checkChenge:function(b,a,t,s){
		if(a[s] == b[s]){
			if(t == "at"){
				return [data.colorMap[a[s]],""];
			}else if(t == "ty"){
				return [a[s],""];
			}else if(t == "sk"){
				return ["",""];
			}
		}else{
			if(t == "at"){
				return [data.colorMap[a[s]],"ev-up"];
			}else if(t == "ty"){
				return [a[s],"ev-up"];
			}else if(t == "sk"){
				return ["","ev-up"];
			}
		}

	}

	,positions:{
		1:[
			[4,2,4],
			[10,110],
			[[100,420],[70,570],[200,520]],
			[[240,380],[250,150],[380,110]],
			[[410,430],[440,570],[570,520]],
			[0,0,0,0]],
		2:[
			[4,2,4,2],
			[10,110],
			[[100,420],[70,570],[200,520]],
			[[240,380],[250,150],[380,110]],
			[[410,430],[440,570],[570,520]],
			[[600,380],[630,150],[760,110]],
			[0,"3:9","4:9","5:9"]],
		3:[
			[3,3],
			[10,310],
			[[250,250],[250,350],[380,310]],
			[[580,250],[580,350],[710,310]],
			[0,"2:9","3:9"]],
		4:[
			[3,2,4,3,1,5],
			[10,310],
			[[260,510],[230,300],[360,310]],
			[[640,320],[620,50],[740,10]],
			[[640,520],[620,670],[740,620]],
			[[650,420],[750,350],[870,310]],
			[[560,320],[250,50],[400,10]],
			[[560,520],[250,670],[400,620]],
			[0,"2:3","2:3:4","2:3:4:5","2:3:4:5:6","2:3:4:k"]],
		5:[
			[4,1,3,4],
			[10,220],
			[[100,560],[80,670],[210,620]],
			[[250,450],[230,100],[360,60]],
			[[550,350],[600,150],[720,110]],
			[[550,440],[600,570],[720,520]],
			[0,"3:4"]],
		6:[
			[2,3,4],
			[10,310],
			[[170,200],[250,50],[380,10]],
			[[240,430],[480,360],[610,310]],
			[[170,640],[250,670],[380,620]],
			[0,"1:2:3"]],
		7:[
			[5,3,1,1,3,4],
			[10,20],
			[[140,420],[10,380],[10,620]],
			[[200,850],[230,660],[360,630]],
			[[250,560],[230,350],[360,320]],
			[[250,260],[230,50],[360,10]],
			[[580,260],[600,50],[730,10]],
			[[580,400],[600,570],[730,520]],
			[0,"5:6"]],
		}
}
