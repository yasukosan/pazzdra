
var leadermap = {

        hoge:""

        ,setuped : false

        ,setup: function(){
            this.showLeaderList();
            if(!this.setuped){
                this.setFilterEvent();
                this.setuped = true;
            }
        }

        ,showLeaderList: function(){

            $("#leader-box").empty();

            for(var i in leader.leaderSkills){
                var ls = leader.leaderSkills[i];
                if(i != 0){
                    if(this.leaderFilter[i] == undefined){
                        this.leaderFilter[i] = 0;
                    }

                    if(this.leaderFilter[i] < 1){
                        $("#leader-box").append("" +
                         "<li>" +
                         " <div>" +
                         " 	<p class='dm-name'>"+ls["Name"]+"</p>" +
                         " 	<p class='dm-skill'>"+ls["Description"]+"</p>" +
                         " </div>" +
                         " <div class='dm-monster-list'>"+this.buildMonsterList(i)+"</div>" +
                         "</li>");
                    }
                }
            }

            return function(){
                leadermap.setViewEvent();
            }();
        }
        ,filter:{
            0:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            1:[0,0,0,0,0,0],
            2:[0,0,0,0,0,0,0,0,0,0,0,0,0]
            }
        ,leaderFilter:[]

        ,setFilterEvent: function(){
            $("#ls-ssearch > li").each(function(){
                $(this).bind("click",function(){
                    var n = $(this).index() + 1;
                    if(!leadermap.filter[0][n]){
                        $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                        leadermap.filter[0][n] = 1;
                        leadermap.filterList(0,n,0);
                    }else{
                        $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                        leadermap.filter[0][n] = 0;
                        leadermap.filterList(0,n,1);
                    }
                })
            })
            $("#ls-asearch > li").each(function(){
                $(this).bind("click",function(){
                    var n = $(this).index() + 1;
                    if(!leadermap.filter[1][n]){
                        $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                        leadermap.filter[1][n] = 1;
                        leadermap.filterList(1,n,0);
                    }else{
                        $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                        leadermap.filter[1][n] = 0;
                        leadermap.filterList(1,n,1);
                    }
                })
            })
            $("#ls-tsearch > li").each(function(){
                $(this).bind("click",function(){
                    var n = $(this).index() + 1;
                    if(!leadermap.filter[2][n]){
                        $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                        leadermap.filter[2][n] = 1;
                        leadermap.filterList(2,n,0);
                    }else{
                        $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                        leadermap.filter[2][n] = 0;
                        leadermap.filterList(2,n,1);
                    }
                })
            })
        }
        ,setType:0
        ,filterList: function(val,num,s){

            for(var i in leader.leaderSkills){
                if(i != 0){
                    var ls = leader.leaderSkills[i];
                    if(val == 0){
                        var fil = ls["Type"].split(":");
                    }
                    if(val == 1){
                        var fil = ls["Attribute"].split(":");
                    }
                    if(val == 2){
                        var fil = ls["MType"].split(":");
                    }

                    if(fil.indexOf(String(num)) < 0){
                    //if($.inArray(num, fil) > 0){
                        if(s){
                            this.leaderFilter[i]--;
                        }else{
                            this.leaderFilter[i]++;
                        }
                    }
                }
            }

            return function(){
                leadermap.showLeaderList();
            }();
        }



        ,buildMonsterList: function(num){
            var ls = leader.leaderSkills[num];

            var check = true;
            var st = 0;
            var list = "";

            while(check){

                var mons = this.showMonster(ls["id"],st);

                if(mons != false){
                    list = list + "<p class='mimage-s' data-role='"+mons+"' ><img src='img/m/"+mons+".jpg'></p>"
                    st = mons;
                }else{
                    check = false;
                }
            }

            return list;
        }

        ,showMonster: function(sk,num){
            var n = monster.leaderMap.indexOf(sk,num);
            if(n > 0){
                return n + 1;
            }else{
                return false;
            }
        }

        ,setViewEvent: function(){
            $("ul#leader-box li").find(".dm-monster-list").each(function(){
                $(this).find("p").each(function(){
                    $(this).bind("click",function(){
                        monster.buildView(this);
                    })
                })
            })
        }

}