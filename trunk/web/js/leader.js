
var leader = {

    hoge:""
    ,leaderSkill	: 0
    ,subLeaderSkill	: 0

    ,comboCount: 0
    ,colorMap: []

    ,HP_Now: 1000
    ,HP_Ori: 1000

    ,Attacke_Now: 0
    ,Attacke_Ori: 0
    ,SubAttacke_Now: 0
    ,SubAttacke_Ori: 0
    ,Cure_Now: 0
    ,Cure_Ori: 0

    ,leaderCheck: [0,0,0,0,0,0]
    ,LF: 0

    ,allReset: function(){
        this.leaderCheck = [0,0,0,0,0,0]
        data.FinalStatus = [
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
    }
    ,getLeaderSkillData:function(){
        AjaxLoader.load(
                "job=getl&id=all",
                'aj',
                "",
                function(target,msg){
                    leader.leaderSkills = eval(msg);
                }
            );
    }

    ,setup: function(){

        this.allReset();

        //this.HP_Ori = hp;
        //this.Attack_Ori = attack;
        //this.SubAttack_Ori = sub_attack;
        //this.Cure_Ori = cure;

        //this.statusResult = $.extend(true,{},this.buildStatus);

    }

    ,leaderSwitch: function(){

        this.setup();

        if(data.PartieList[0][0]["Reader"] != undefined
            && data.PartieList[0][0]["Reader"] > 0){
            leader.LF = 0;
            this.loadProgram(true);
        }
        return function(){

            if(data.PartieList[0][5]["Reader"] != undefined
                    && data.PartieList[0][5]["Reader"] > 0){
                leader.LF = 1;
                leader.loadProgram(false);
            }

            return function(){
                leader.LF = 0;
                leader.setView();
            }();

        }();
    }

    ,loadProgram: function(flag){
        if(flag){
            var sk = this.leaderSkills[data.PartieList[0][0]["Reader"]];
        }else{
            var sk = this.leaderSkills[data.PartieList[0][5]["Reader"]];
        }

        var skill = "type"+sk["Program"];
        var param = sk["Propatie"].split(",");

        try {
            leader[skill](param);
        } catch (e) {
            alert(e);
        }

    }

    ,setView: function(){
        var hp = 0;
        var attack = 0;
        var sub_attack = 0;
        var cure = 0;

        for(var i in data.LeaderResult[0]){
            hp = hp + Number(data.LeaderResult[0][i]["HP"]);
            attack = attack + Number(data.LeaderResult[0][i]["Attack"]);
            sub_attack = sub_attack + Number(data.LeaderResult[0][i]["SubAttack"]);
            cure = cure + Number(data.LeaderResult[0][i]["Cure"]);
        }

        //this.HP_Ori = hp;this.HP_Now = hp;
        //this.Attack_Ori = attack;this.Attack_Now = attack;
        //this.SubAttack_Ori = sub_attack;this.SubAttack_Now = sub_attack;
        //this.Cure_Ori = cure;this.Cure_Now = cure;
    }


    ,checkType: function(a,t,type,s,n){
        for(var i in data.PartieResult[0]){
            var re = data.PartieResult[0][i];

            if(type == 0){
                if(a.indexOf(re["Attribute"]) > 0
                        || re["SubAttribute"] > 0
                        && a.indexOf(re["SubAttribute"]) > 0){
                    this.calcStatus(s,n,i);
                }
            }else if(type == 1){
                if(t.indexOf(re["Type"]) > 0
                        || re["SubType"] > 0
                        && t.indexOf(re["SubType"]) > 0){
                    this.calcStatus(s,n,i);
                }
            }else if(type == 2){
                if(a.indexOf(re["Attribute"]) > 0
                        || re["SubAttribute"] > 0
                        && a.indexOf(re["SubAttribute"]) > 0
                        ||t.indexOf(re["Type"]) > 0
                        || re["SubType"] > 0
                        && t.indexOf(re["SubType"]) > 0){
                    this.calcStatus(s,n,i);
                }
            }
        }
    }
    ,calcStatus: function(status,num,i){

        var re = data.LeaderResult[0][i];
        var pr = data.PartieResult[0][i];

        for(var j in status){
            if(status[j] != "0" && status[j] != undefined){
                if(status[j] == "Attack" && re["SubAttribute"] != 0){
                    if(re["Attribute"] == re["SubAttribute"]){
                        re["SubAttack"] = Math.ceil(pr[status[j]] * 0.1) * num;
                    }else{
                        re["SubAttack"] = Math.ceil(pr[status[j]] * 0.3) * num;
                    }
                }else{
                    re[status[j]] =pr[status[j]] * num;
                }

                var da = data.FinalStatus[i];
                var val = [[8,9,10],[11,12,13]];
                var v = leader.LF;

                if(status[j] == "Attack"){
                    if(da[val[v][0]] == 0){
                        da[val[v][0]] = num;
                    }else{
                        da[val[v][0]] = da[val[v][0]] * num;
                    }
                }else if(status[j] == "HP"){
                    if(da[val[v][1]] == 0){
                        da[val[v][1]] = num;
                    }else{
                        da[val[v][1]] = da[val[v][1]] * num;
                    }
                }else if(status[j] == "Cure"){
                    if(da[val[v][2]] == 0){
                        da[val[v][2]] = num;
                    }else{
                        da[val[v][2]] = da[val[v][2]] * num;
                    }
                }

            }
        }
    }
    /**=======================================================
     *
     *
     * 				タイプ、属性変動型
     *
     *
     =========================================================*/
    /*
     * 特定の属性でステータスアップ
     * */
    ,type101:function(p){

        var a = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        this.checkType(a,0,0,s,n);
    }

    /*特定のタイプでステータスアップ*/
    ,type102:function(p){
        var t = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        this.checkType(0,t,1,s,n);
    }

    /*特定の属性とタイプでステータスアップ*/
    ,type103:function(p){
        var a = p[0].split(':');
        var t = p[1].split(':');
        var s = p[2].split(':');
        var n = Number(p[3]);

        this.checkType(a,t,2,s,n);

    }

    /*特定の属性のステータスが上がり、他のステータスも上がる*/
    ,type104:function(p){

        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var status2 = p[3].split(':');
        var num2 = Number(p[4]);

        for(var i in data.LeaderResult[0]){

            var re = data.LeaderResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0
                && attribute.indexOf(re["SubAttribute"]) > 0){
                this.calcStatus(status,num,i);

                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*特定のタイプのステータスが上がり、他のステータスも上がる*/
    ,type105:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var status2 = p[3].split(':');
        var num2 = Number(p[4]);

        for(var i in data.LeaderResult[0]){

            var re = data.LeaderResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && type.indexOf(re["SubType"]) > 0){

                this.calcStatus(status,num,i);

                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*特定の属性と特定タイプのステータスが上がり、他のステータスも上がる*/
    ,type106:function(p){
        var attribute = p[0].split(':');
        var type = p[1].split(':');
        var status = p[2].split(':');
        var num = Number(p[3]);
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);

        for(var i in data.LeaderResult[0]){

            var re = data.LeaderResult[0][i];

            if(attribute.indexOf(re["Attirbute"]) > 0
                || re["SubAttribute"] > 0
                && attribute.indexOf(re["SubAttribute"] > 0)
                || type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && type.indexOf(re["SubType"]) > 0){
                this.calcStatus(status,num,i);

                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*特定属性のステータスが上がり、他の属性のステータスが上がる*/
    ,type107:function(p){
        var attribute = p[0].split(':');
        var type = p[1].split(':');
        var status = p[2].split(':');
        var num = Number(p[3]);
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attirbute"]) > 0
                || re["SubAttribute"] > 0
                && attribute.indexOf(re["SubAttribute"]) > 0){
                this.calcStatus(status,num,i);

                this.calcStatus(status2,num2,i);
            }

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && type.indexOf(re["SubType"]) > 0){
                this.calcStatus(status,num,i);

                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*特定属性のステータスがあがり、指定コンボ以上でステータスが上がる*/
    ,type180:function(p){
        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var combo = Number(p[3]);
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0
                && attribute.indexOf(re["SubAttribute"]) > 0){
                this.calcStatus(status,num,i);
            }

            if(pazzle.comboCount >= combo){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*特定タイプのステータスがあがり、指定コンボ以上でステータスが上がる*/
    ,type181:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var combo = Number(p[3]);
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && type.indexOf(re["SubType"]) > 0){
                this.calcStatus(status,num,i);
            }

            if(pazzle.comboCount >= combo){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /**
     * 特定属性のステータスがあがる
     * 特定属性のステータスがあがる、
     * 両方の属性を持っている場合ステータスが上がる
     * */
    ,type182:function(p){
        var attribute = p[0].split(':');
        var attribute2 = p[1].split(':');
        var status = p[2].split(':');
        var status2 = p[3].split(':');
        var status3 = p[4].split(':');
        var num = Number(p[5]);
        var num2 = Number(p[6]);
        var num3 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(
                    re["SubAttribute"] > 0
                    && attribute.indexOf(re["Attribute"]) > 0
                    && attribute2.indexOf(re["SubAttribute"]) > 0
                    || re["SubAttribute"] > 0
                    && attribute2.indexOf(re["Attribute"]) > 0
                    && attribute.indexOf(re["SubAttribute"]) > 0){
                        this.calcStatus(status3,num3,i);

            }else if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0
                && attribute.indexOf(re["SubAttribute"] > 0)){
                this.calcStatus(status,num,i);

            }else if(attribute2.indexOf(re["Attribute"]) > 0
                    || re["SubAttribute"] > 0
                    && attribute2.indexOf(re["SubAttribute"] > 0)){
                    this.calcStatus(status2,num2,i);
            }


        }
    }

    /**
     * 特定属性のステータスがあがり、
     * 特定属性の組み合わせ同時攻撃でステータスアップ
     * */
    ,type183:function(p){
        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var drop = p[3].split(':');
        var count = Number(p[4]);
        var status2 = p[5].split(':');
        var num2 = Number(p[6]);

        var dropCount = 1;
        var dropMap = [];

        for(var d in drop){
            if($.inArray(Number(drop[d]), pazzle.colorMap) >= 0){
                if(dropMap.indexOf(drop[d]) <= 0){
                    dropMap.push(drop[d]);
                }
            }
        }
        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                    || re["SubAttribute"] > 0
                    && attribute.indexOf(re["SubAttribute"] > 0)){
                    this.calcStatus(status,num,i);
            }

            if(dropMap.indexOf(re["Attribute"]) > 0){
                dropCount++;
            }
            if(re["SubAttribute"] > 0 && dropMap.indexOf(re["SubAttribute"]) > 0){
                dropCount++;
            }
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /**
     * 特定タイプのステータスがあがり、
     * 特定属性の組み合わせ同時攻撃でステータスアップ
     * */
    ,type184:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var drop = p[0].split(':');
        var count = Number(p[1]);
        var status2 = p[2].split(':');
        var num2 = Number(p[3]);

        var dropCount = 1;
        var dropMap = [];

        for(var d in drop){
            if($.inArray(Number(drop[d]), pazzle.colorMap) >= 0){
                if(dropMap.indexOf(drop[d]) <= 0){
                    dropMap.push(drop[d]);
                }
            }
        }
        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0
                    && type.indexOf(re["SubType"] > 0)){
                    this.calcStatus(status,num,i);
            }

            if(dropMap.indexOf(re["Attribute"]) > 0){
                dropCount++;
            }
            if(re["SubAttribute"] > 0 && dropMap.indexOf(re["SubAttribute"]) > 0){
                dropCount++;
            }
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /**
     * 特定タイプのステータスがあがり、
     * ふんばる
     * */
    ,type185:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];
            if(type.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0
                    && type.indexOf(re["SubType"]) > 0){
                    this.calcStatus(status,num,i);
            }
        }
    }
    /**
     * 特定タイプのステータスがあがり、
     * 指定属性数の同時攻撃でステータスアップ
     * */
    ,type186:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var count = Number(p[3]);
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);
        var cure = Number(p[6]);

        var dropCount = 1;
        var dropMapCheck = [];

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0
                    && type.indexOf(re["SubType"] > 0)){
                    this.calcStatus(status,num,i);
            }

            if($.inArray(Number(re["Attribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropMapCheck.push(re["Attribute"]);
                dropCount++;
            }

            if(re["SubAttribute"] > 0
                && $.inArray(Number(re["SubAttribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["SubAttribute"]) < 0){
                dropMapCheck.push(re["SubAttribute"]);
                dropCount++;
            }
        }

        if(cure == 1 && $.inArray(6, pazzle.colorMap) >= 0){
            dropCount++;
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                leader.calcStatus(status2,num2,i);
            }
        }
    }

    /**
     * 特定タイプのステータスがあがり、
     * 指定色のドロップを指定子繋げると、ステータスアップ
     * */
    ,type187:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var color = p[3].split(':');
        var count = Number(p[4]);
        var status2 = p[5].split(':');
        var num2 = Number(p[6]);

        var dropCount = 1;
        var dropMapCheck = [];

        var c = false;

        for(var i in color){

            if($.inArray(Number(color[i]), pazzle.colorMap) >= 0){
                for(var j in pazzle.combo[data.colorMap[color[i]]]){
                    if(pazzle.combo[data.colorMap[color[i]]][j][0] >= count){
                        c = true;
                    }
                }
            }
        }

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0
                    && type.indexOf(re["SubType"] > 0)){
                    this.calcStatus(status,num,i);
            }

            if(c){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /**
     * 特定のタイプでステータスアップ
     * ドロップ操作時間延長
     * */
    ,type188:function(p){
        var t = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        this.checkType(0,t,1,s,n);
    }

    /**
     * 特定属性のステータスがあがり、
     * 指定色のドロップを指定子繋げると、ステータスアップ
     * */
    ,type189:function(p){
        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var color = p[3].split(':');
        var count = Number(p[4]);
        var status2 = p[5].split(':');
        var num2 = Number(p[6]);

        var dropCount = 1;
        var dropMapCheck = [];

        var c = false;

        for(var i in color){

            if($.inArray(Number(color[i]), pazzle.colorMap) >= 0){
                for(var j in pazzle.combo[data.colorMap[color[i]]]){
                    if(pazzle.combo[data.colorMap[color[i]]][j][0] >= count){
                        c = true;
                    }
                }
            }
        }

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                    || re["SubAttribute"] > 0
                    && attribute.indexOf(re["SubAttribute"] > 0)){
                    this.calcStatus(status,num,i);
            }

            if(c){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /**
     *
     * 特定属性のステータスがあがり
     * 特定属性の指定数コンボ以上でステータスアップ
     *
     * */
    ,type190:function(p){
        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var color = p[3].split(':');
        var count = Number(p[4]);
        var max =  Number(p[5]);
        var status2 = p[6].split(':');
        var num2 = Number(p[7]);

        var dropCount = 0;
        var cname = data.colorMap[Number(color[1])];

        dropCount = pazzle.combo[cname].length;

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                    || re["SubAttribute"] > 0
                    && attribute.indexOf(re["SubAttribute"] > 0)){
                    this.calcStatus(status,num,i);
            }

            if(dropCount >= count){
                var over = dropCount - count;

                if(over > max){
                    over = max;
                }
                var n = num2 + (up * over);
                this.calcStatus(status2,n,i);
            }
        }
    }

    /**
    *
    * 特定タイプのステータスがあがり
    * 特定属性の指定数コンボ以上でステータスアップ
    *
    * */
   ,type191:function(p){
       var type = p[0].split(':');
       var status = p[1].split(':');
       var num = Number(p[2]);
       var color = p[3].split(':');
       var count = Number(p[4]);
       var max =  Number(p[5]);
       var status2 = p[6].split(':');
       var num2 = Number(p[7]);

       var dropCount = 0;
       var cname = data.colorMap[Number(color[1])];

       dropCount = pazzle.combo[cname].length;

       for(var i in data.PartieResult[0]){

           var re = data.PartieResult[0][i];

           if(type.indexOf(re["Type"]) > 0
                   || re["SubType"] > 0
                   && ytpe.indexOf(re["SubType"] > 0)){
                   this.calcStatus(status,num,i);
           }

           if(dropCount >= count){
               var over = dropCount - count;

               if(over > max){
                   over = max;
               }
               var n = num2 + (up * over);
               this.calcStatus(status2,n,i);
           }
       }
   }

   /**
    * 特定属性のステータスがあがり、
    * 別のステータスも上がり
    * 指定色のドロップを指定子繋げると、ステータスアップ
    * */
   ,type192:function(p){
       var attribute = p[0].split(':');
       var status = p[1].split(':');
       var num = Number(p[2]);
       var attribute2 = p[0].split(':');
       var status2 = p[1].split(':');
       var num2 = Number(p[2]);
       var color = p[3].split(':');
       var count = Number(p[4]);
       var status3 = p[5].split(':');
       var num3 = Number(p[6]);

       var dropCount = 1;
       var dropMapCheck = [];

       var c = false;

       for(var i in color){

           if($.inArray(Number(color[i]), pazzle.colorMap) >= 0){
               for(var j in pazzle.combo[data.colorMap[color[i]]]){
                   if(pazzle.combo[data.colorMap[color[i]]][j][0] >= count){
                       c = true;
                   }
               }
           }
       }

       for(var i in data.PartieResult[0]){

           re = data.PartieResult[0][i];

           if(attribute.indexOf(re["Attribute"]) > 0
                   || re["SubAttribute"] > 0
                   && attribute.indexOf(re["SubAttribute"] > 0)){
                   this.calcStatus(status,num,i);
           }
           if(attribute2.indexOf(re["Attribute"]) > 0
                   || re["SubAttribute"] > 0
                   && attribute2.indexOf(re["SubAttribute"] > 0)){
                   this.calcStatus(status2,num2,i);
           }

           if(c){
               this.calcStatus(status3,num3,i);
           }
       }
   }

   /**
    * 特定タイプのステータスがあがり、
    * 別のステータスも上がり
    * 指定色のドロップを指定子繋げると、ステータスアップ
    * */
   ,type193:function(p){
       var type = p[0].split(':');
       var status = p[1].split(':');
       var num = Number(p[2]);
       var type2 = p[0].split(':');
       var status2 = p[1].split(':');
       var num2 = Number(p[2]);
       var color = p[3].split(':');
       var count = Number(p[4]);
       var status3 = p[5].split(':');
       var num3 = Number(p[6]);

       var dropCount = 1;
       var dropMapCheck = [];

       var c = false;

       for(var i in color){

           if($.inArray(Number(color[i]), pazzle.colorMap) >= 0){
               for(var j in pazzle.combo[data.colorMap[color[i]]]){
                   if(pazzle.combo[data.colorMap[color[i]]][j][0] >= count){
                       c = true;
                   }
               }
           }
       }

       for(var i in data.PartieResult[0]){

           re = data.PartieResult[0][i];

           if(type.indexOf(re["Type"]) > 0
                   || re["SubType"] > 0
                   && type.indexOf(re["SubType"] > 0)){
                   this.calcStatus(status,num,i);
           }
           if(type2.indexOf(re["Type"]) > 0
                   || re["SubType"] > 0
                   && type2.indexOf(re["SubType"] > 0)){
                   this.calcStatus(status2,num2,i);
           }

           if(c){
               this.calcStatus(status3,num3,i);
           }
       }
   }

    /**=======================================================
    *
    *
    * 				HP 変動型
    *
    *
    =========================================================*/
    /*HPが特定条件時、ステータスアップ*/
    ,type201:function(p){

        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(up == "up"){
                if((hp2 / hp1) >= filter){
                    this.calcStatus(status,num,i);
                }
            }else{
                if((hp2 / hp1) <= filter){
                    this.calcStatus(status,num,i);
                }
            }
        }
    }

    /*HPが特定条件時、特定属性のステータスアップ*/
    ,type202:function(p){

        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var attribute = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0
                && attribute.indexOf(re["SubAttribute"]) > 0){

                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status,num,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status,num,i);
                    }
                }
            }
        }
    }

    /*HPが特定条件時、特定タイプのステータスアップ*/
    ,type203:function(p){

        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var type = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && type.indexOf(re["SubType"]) > 0){

                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status,num,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status,num,i);
                    }
                }
            }
        }
    }

    /*HPが特定条件時、特定タイプのステータスアップ、特定タイプの別ステータスアップ*/
    ,type204:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var type = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);

        var type2 = p[5].split(':');
        var status2 = p[6].split(':');
        var num2 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && type.indexOf(re["SubType"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status,num,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status,num,i);
                    }
                }
            }
            if(type2.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && type2.indexOf(re["SubType"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status2,num2,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status2,num2,i);
                    }
                }
            }
        }
    }
    /*HPが特定条件時、特定属性のステータスアップ、特定属性の別ステータスアップ*/
    ,type205:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var attribute = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);
        var attribute2 = p[5].split(':');
        var status2 = p[6].split(':');
        var num2 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status,num,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status,num,i);
                    }
                }
            }
            if(attribute2.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute2.indexOf(re["SubAttribute"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status2,num2,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status2,num2,i);
                    }
                }
            }
        }
    }

    /**
     * HPの状態に応じて、特定タイプのステータスアップ
     * （3段階で変動）
     *
     * 特定タイプのステータスがアップ
     * */
    ,type206:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter1 = Number(p[1]);
        var filter2 = Number(p[2]);
        var filter3 = Number(p[3]);
        var type = p[4].split(':');
        var status = p[5].split(':');
        var num1 = Number(p[6]);
        var num2 = Number(p[7]);
        var num3 = Number(p[8]);
        var type2 = p[9].split(':');
        var status2 = p[10].split(':');
        var num4 = Number(p[11]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && type.indexOf(re["SubType"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter1){
                        this.calcStatus(status,num1,i);
                    }else if((hp2 / hp1) >= filter2){
                        this.calcStatus(status,num2,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status,num,i);
                    }
                }
            }

            if(type2.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0
                    && type2.indexOf(re["SubType"]) > 0){
                    this.calcStatus(status2,num4,i);
            }
        }

    }

    /**
     * HPの状態に応じて、特定属性のステータスアップ
     * （3段階で変動）
     *
     * 特定属性のステータスがアップ
     * */
    ,type207:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter1 = Number(p[1]);
        var filter2 = Number(p[2]);
        var filter3 = Number(p[3]);
        var attribute = p[4].split(':');
        var status = p[5].split(':');
        var num1 = Number(p[6]);
        var num2 = Number(p[7]);
        var num3 = Number(p[8]);
        var attribute2 = p[9].split(':');
        var status2 = p[10].split(':');
        var num4 = Number(p[11]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter1){
                        this.calcStatus(status,num1,i);
                    }else if((hp2 / hp1) >= filter2){
                        this.calcStatus(status,num2,i);
                    }else if((hp2 / hp1) >= filter3){
                        this.calcStatus(status,num3,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter1){
                        this.calcStatus(status,num1,i);
                    }else if((hp2 / hp1) <= filter2){
                        this.calcStatus(status,num2,i);
                    }else if((hp2 / hp1) <= filter3){
                        this.calcStatus(status,num3,i);
                    }
                }
            }

            if(attribute2.indexOf(re["Attribute"]) > 0
                    || re["SubAttribute"] > 0
                    && attribute2.indexOf(re["SubAttribute"]) > 0){
                    this.calcStatus(status2,num4,i);
            }
        }

    }

    /*特定属性のステータスが上がり、HPが特定条件時、特定タイプのステータスアップ*/
    ,type281:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var attribute = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);

        var type = p[5].split(':');
        var status2 = p[6].split(':');
        var num2 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){

                    this.calcStatus(status,num,i);
            }

            if(type.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0 && type.indexOf(re["SubType"]) > 0){
                    if(up == "up"){
                        if((hp2 / hp1) >= filter){
                            this.calcStatus(status2,num2,i);
                        }
                    }else{
                        if((hp2 / hp1) <= filter){
                            this.calcStatus(status2,num2,i);
                        }
                    }
            }
        }
    }
    /*特定属性のステータスが上がり、HPが特定条件時、特定属性のステータスアップ*/
    ,type282:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var attribute = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);
        var attribute2 = p[5].split(':');
        var status2 = p[6].split(':');
        var num2 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){

                    this.calcStatus(status,num,i);
            }
            if(attribute2.indexOf(re["Attribute"]) > 0
                    || re["SubAttribute"] > 0 && attribute2.indexOf(re["SubAttribute"]) > 0){
                    if(up == "up"){
                        if((hp2 / hp1) >= filter){
                            this.calcStatus(status2,num2,i);
                        }
                    }else{
                        if((hp2 / hp1) <= filter){
                            this.calcStatus(status2,num2,i);
                        }
                    }
            }
        }
    }
    /*特定タイプのステータスが上がり、HPが特定条件時、特定タイプのステータスアップ*/
    ,type283:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var type = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);
        var type2 = p[5].split(':');
        var status2 = p[6].split(':');
        var num2 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && type.indexOf(re["SubType"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status,num,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status,num,i);
                    }
                }
            }
            if(type2.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0 && type2.indexOf(re["SubType"]) > 0){
                    if(up == "up"){
                        if((hp2 / hp1) >= filter){
                            this.calcStatus(status2,num2,i);
                        }
                    }else{
                        if((hp2 / hp1) <= filter){
                            this.calcStatus(status2,num2,i);
                        }
                    }
            }
        }
    }

    /**
     *
     * HPが特定条件時、特定属性のステータスアップ
     * チーム内に特定モンスターがいる場合ステータスアップ
     *
     * */
    ,type284:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter = Number(p[1]);
        var attribute = p[2].split(':');
        var status = p[3].split(':');
        var num = Number(p[4]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attirbute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && type.indexOf(re["SubAttribute"]) > 0){
                if(up == "up"){
                    if((hp2 / hp1) >= filter){
                        this.calcStatus(status,num,i);
                    }
                }else{
                    if((hp2 / hp1) <= filter){
                        this.calcStatus(status,num,i);
                    }
                }
            }
        }
    }

    /**
     * HPの状態に応じて、特定属性のステータスアップ
     * （3段階で変動）
     *
     * ソロップ操作時間延長
     * */
    ,type285:function(p){
        var hp1 = this.HP_Ori;
        var hp2 = this.HP_Now;

        var up = p[0];
        var filter1 = Number(p[1]);
        var filter2 = Number(p[2]);
        var filter3 = Number(p[3]);
        var status = p[4].split(':');
        var num1 = Number(p[5]);
        var num2 = Number(p[6]);
        var num3 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(up == "up"){
                if((hp2 / hp1) >= filter1){
                    this.calcStatus(status,num1,i);
                }else if((hp2 / hp1) >= filter2){
                    this.calcStatus(status,num2,i);
                }else if((hp2 / hp1) >= filter3){
                    this.calcStatus(status,num3,i);
                }
            }else{
                if((hp2 / hp1) <= filter1){
                    this.calcStatus(status,num1,i);
                }else if((hp2 / hp1) <= filter2){
                    this.calcStatus(status,num2,i);
                }else if((hp2 / hp1) <= filter3){
                    this.calcStatus(status,num3,i);
                }
            }
        }

    }
    /**=======================================================
    *
    *
    * 				特定ステータス犠牲型
    *
    *
    =========================================================*/
    /*特定ステータスが変動し、特定属性のステータスアップ*/
    ,type301:function(status,num,attirbu){

        var status = p[0].split(':');
        var num = Number(p[1]);
        var attribute = p[2].split(':');
        var status2 = p[3].split(':');
        var num = Number(p[4]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            for(var j in status){
                if(status[j] != "0" && status[j] != undefined){
                    re[status[j]] =re[status[j]] * num;
                }
            }

            if(attirbute.indexOf(re["Attribute"]) > 0
                || re["SubAttirbute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){

                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*特定タイプのステータスが変動し、特定タイプのステータスアップ*/
    ,type302:function(){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var type2 = p[3].split(':');
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && type.indexOf(re["SubType"]) > 0){
                this.calcStatus(status,num,i);
            }

            if(type2.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && type2.indexOf(re["SubType"]) > 0){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*特定属性のステータスが変動し、特定属性のステータスアップ*/
    ,type303:function(){
        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var attribute2 = p[3].split(':');
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){
                this.calcStatus(status,num,i);
            }

            if(attribute2.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute2.indexOf(re["SubAttribute"]) > 0){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /**=======================================================
    *
    *
    * 				特定ドロップの同時消しでステータスアップ
    *
    *
    =========================================================*/

    /*特定属性の同時攻撃でステータスアップ*/
    ,type401:function(p){
        var drop = p[0].split(':');
        var count = Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);

        var dropCount = 1;
        var dropMap = [];
        var dropMapCheck = [];

        for(var d in drop){
            if($.inArray(Number(drop[d]), pazzle.colorMap) >= 0){
                if(dropMap.indexOf(drop[d]) <= 0){
                    dropMap.push(drop[d]);
                }
            }
        }
        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(dropMap.indexOf(re["Attribute"]) > 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropCount++;
            }
            if(re["SubAttribute"] > 0
                && dropMap.indexOf(re["SubAttribute"]) > 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropCount++;
            }
        }

        if($.inArray(6, pazzle.colorMap) >= 0){
            dropCount++;
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }
    }

    /*特定属性の同時攻撃でステータスアップし　指定攻撃数でステータスアップ*/
    ,type402:function(p){
        var drop = p[0].split(':');
        var count = Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);
        var count2 = Number(p[4]);
        var status2 = p[5].split(':');
        var num2 = Number(p[6]);

        var dropCount = 1;
        var dropMap = [];
        var dropMapCheck = [];

        for(var d in drop){
            if($.inArray(Number(drop[d]), pazzle.colorMap) >= 0){
                if(dropMap.indexOf(drop[d]) < 0){
                    dropMap.push(drop[d]);
                }
            }
        }
        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(dropMap.indexOf(re["Attribute"]) > 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropMapCheck.push(re["Attribute"]);
                dropCount++;
            }
            if(re["SubAttribute"] > 0
                && dropMap.indexOf(re["SubAttribute"]) > 0
                && dropMapCheck.indexOf(re["SubAttribute"]) < 0){
                dropMapCheck.push(re["SubAttribute"]);
                dropCount++;
            }
        }
/*
        if($.inArray(6, pazzle.colorMap) > 0){
            dropCount++;
        }
*/
        if(dropCount == count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }else if(dropCount == count2){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status2,num2,i);
            }
        }

    }

    /*一定属性数で同時攻撃する時、ステータスアップ*/
    ,type403:function(p){
        var count = Number(p[0]);
        var status = p[1].split(':');
        var num = Number(p[2]);
        var cure = Number(p[3]);

        var dropCount = 1;
        var dropMapCheck = [];

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if($.inArray(Number(re["Attribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropMapCheck.push(re["Attribute"]);
                dropCount++;
            }

            if(re["SubAttribute"] > 0
                && $.inArray(Number(re["SubAttribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["SubAttribute"]) < 0){
                dropMapCheck.push(re["SubAttribute"]);
                dropCount++;
            }
        }

        if(cure == 1 && $.inArray(6, pazzle.colorMap) >= 0){
            dropCount++;
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                leader.calcStatus(status,num,i);
            }
        }

    }


    /*特定の属性のステータスアップ、一定属性数で同時攻撃する時、ステータスアップ*/
    ,type405:function(p){
        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var count = Number(p[3]);
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);
        var cure = Number(p[6]);

        var dropCount = 1;
        var dropMapCheck = [];

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(attribute.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){
                this.calcStatus(status,num,i);
            }


            if($.inArray(Number(re["Attribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropMapCheck.push(re["Attribute"]);
                dropCount++;
            }

            if(re["SubAttribute"] > 0
                && $.inArray(Number(re["SubAttribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["SubAttribute"]) < 0){
                dropMapCheck.push(re["SubAttribute"]);
                dropCount++;
            }
        }
        if(cure == 1 && $.inArray(6, pazzle.colorMap) >= 0){
            dropCount++;
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /*同時に消した属性の数でステータスアップ*/
    ,type406:function(p){
        var count = Number(p[0]);
        var max =  Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);
        var up =  Number(p[4]);
        var cure = Number(p[5]);

        var dropCount = 1;
        var dropMapCheck = [];

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if($.inArray(Number(re["Attribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropMapCheck.push(re["Attribute"]);
                dropCount++;
            }
            if(re["SubAttribute"] > 0
                && $.inArray(Number(re["SubAttribute"]), pazzle.colorMap) >= 0
                && dropMapCheck.indexOf(re["SubAttribute"]) < 0){
                dropMapCheck.push(re["SubAttribute"]);
                dropCount++;
            }
        }

        if(cure == 1 && $.inArray(6, pazzle.colorMap) >= 0){
            dropCount++;
        }

        if(dropCount >= count){

            var over = dropCount - count;

            if(over > max){
                over = max;
            }

            var n = num + (up * over);

            for(var i in data.PartieResult[0]){
                this.calcStatus(status,n,i);
            }
        }
    }




    /*特定のタイプのステータスアップ、特定属性で同時攻撃する時、ステータスアップ*/
    ,type481:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var drop = p[3].split(':');
        var count = Number(p[4]);
        var status2 = p[5].split(':');
        var num2 = Number(p[6]);

        var dropCount = 1;

        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(type.indexOf(re["Type"]) > 0
                    || re["SubType"] > 0
                    && type.indexOf(re["SubType"] > 0)){

                    this.calcStatus(status,num,i);
                }


            if($.inArray(Number(re["Attribute"]), pazzle.colorMap) >= 0){
                dropCount++;
            }
            if(re["SubAttribute"] > 0
                && $.inArray(Number(re["SubAttribute"]), pazzle.colorMap) >= 0){
                dropCount++;
            }
        }

        if($.inArray(6, pazzle.colorMap) >= 0){
            dropCount++;
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status2,num2,i);
            }
        }
    }

    /**
     * 特定属性の同時攻撃でステータスアップし
     * 一定コンボ以上でステータスアップ
     * */
    ,type482:function(p){
        var drop = p[0].split(':');
        var count = Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);
        var count2 = Number(p[4]);
        var status2 = p[5].split(':');
        var num2 = Number(p[6]);

        var dropCount = 1;
        var dropMap = [];
        var dropMapCheck = [];

        for(var d in drop){
            if($.inArray(Number(drop[d]), pazzle.colorMap) >= 0){
                if(dropMap.indexOf(drop[d]) <= 0){
                    dropMap.push(drop[d]);
                }
            }
        }
        for(var i in data.PartieResult[0]){

            re = data.PartieResult[0][i];

            if(dropMap.indexOf(re["Attribute"]) > 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropCount++;
            }
            if(re["SubAttribute"] > 0
                && dropMap.indexOf(re["SubAttribute"]) > 0
                && dropMapCheck.indexOf(re["Attribute"]) < 0){
                dropCount++;
            }

            if(pazzle.comboCount >= count){
                for(var i in data.PartieResult[0]){
                    this.calcStatus(status2,num2,i);
                }
            }
        }

        if($.inArray(6, pazzle.colorMap) >= 0){
            dropCount++;
        }

        if(dropCount >= count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }
    }


    /**=======================================================
    *
    *
    * 				一定コンボ以上でステータスアップ
    *
    *
    =========================================================*/

    /*指定コンボ以上でステータスアップ*/
    ,type501:function(count,status,num){
        var count = Number(p[0]);
        var status = p[1].split(':');
        var num = Number(p[2]);

        if(pazzle.comboCount >= count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }

    }

    /*指定コンボ以上でステータスアップ、指定率まで上がる*/
    ,type502:function(p){
        var count = Number(p[0]);
        var status = p[1].split(':');
        var num = Number(p[2]);
        var up =  Number(p[3]);
        var max =  Number(p[4]);

        if(pazzle.comboCount >= count){

            var over = pazzle.comboCount - count;
            var n = num + (n * over);

            if(n >= max){
                n = max;
            }



            for(var i in data.PartieResult[0]){
                this.calcStatus(status,n,i);
            }
        }
    }

    /*指定コンボ以上で、特定属性のステータスアップ*/
    ,type503:function(p){
        var attribute = p[0].split(':')
        var count = Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);

        if(pazzle.comboCount >= count){

            var over = pazzle.comboCount - count;
            var n = num + (up * over);

            for(var i in data.PartieResult[0]){
                var re = data.PartieResult[0][i];
                if(attirbute.indexOf(re["Attribute"]) > 0
                    || re["SubAttirbute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){

                    this.calcStatus(status,num,i);
                }
            }
        }
    }

    /*指定コンボ丁度でステータスアップ*/
    ,type504:function(p){
        var count = Number(p[0]);
        var status = p[1].split(':');
        var num = Number(p[2]);

        if(pazzle.comboCount == count){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }
    }

    /*特定属性のステータスアップ、指定コンボ以上でステータスアップ*/
    ,type505:function(p){
        var attribute = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var count = Number(p[3]);
        var status2 = p[4].split(':');
        var num2 = Number(p[5]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];

            if(attirbute.indexOf(re["Attribute"]) > 0
                || re["SubAttirbute"] > 0 && attribute.indexOf(re["SubAttribute"]) > 0){

                this.calcStatus(status,num,i);
            }

            if(pazzle.comboCount >= count){
                this.calcStatus(status2,num2,i);
            }
        }

    }

    /*特定属性の指定コンボ以上でステータスアップ*/
    ,type506:function(p){
        var color = p[0].split(':');
        var count = Number(p[1]);
        var max =  Number(p[2]);
        var status = p[3].split(':');
        var num = Number(p[4]);
        var up =  Number(p[5]);

        var dropCount = 0;
        var cname = data.colorMap[Number(color[1])];

        dropCount = pazzle.combo[cname].length;

        if(dropCount >= count){

            var over = dropCount - count;

            if(over > max){
                over = max;
            }

            var n = num + (up * over);

            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }

    }

    /*特定属性の組み合わせでステータスアップし、特定属性の組み合わせでステータスアップ*/
    ,type507:function(p){
        var color1 = p[0].split(':');
        var color2 = p[1].split(':');
        var status = p[2].split(':');
        var num = Number(p[3]);
        var num2 = Number(p[4]);

        var cname = data.colorMap[Number(color[1])];

        dropCount = pazzle.combo[cname].length;
        for(var i in pazzle.colorMap){
            if(color1.indexOf(pazzle.colorMap[i]) >= 0){
                color1.shift();
            }
            if(color2.indexOf(pazzle.colorMap[i]) >= 0){
                color2.shift();
            }
        }

        if(color1.lenght <= 0
            && color2.lenght <= 0){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num2,i);
            }
        }else if(color1.lenght <= 0){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }
    }

    /**
     * 特定属性の組み合わせか、特定属性の組み合わせでステータスがあがり
     * 最大指定倍まで上がる
     * */
    ,type508:function(p){
        var color1 = p[0].split(':');
        var color2 = p[1].split(':');
        var status = p[2].split(':');
        var num = Number(p[3]);

        for(var i in pazzle.colorMap){
            if(color1.indexOf(pazzle.colorMap[i]) > 0){
                color1.shift();
            }
            if(color2.indexOf(pazzle.colorMap[i]) > 0){
                color2.shift();
            }
        }

        if(color1.lenght <= 0
            || color2.lenght <= 0){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }
    }

    /**
     *
     * 特定タイプのステータスが上がり
     * 特定属性の組み合わせでステータスアップし、
     * 特定属性の組み合わせでステータスアップ
     *
     * */
    ,type581:function(p){
        var type = p[0].split(':');
        var status = p[1].split(':');
        var num = Number(p[2]);
        var color1 = p[3].split(':');
        var color2 = p[4].split(':');
        var status2 = p[5].split(':');
        var num2 = Number(p[6]);
        var num3 = Number(p[7]);

        for(var i in data.PartieResult[0]){

            var re = data.PartieResult[0][i];
            if(attirbute.indexOf(re["Type"]) > 0
                || re["SubType"] > 0 && attribute.indexOf(re["SubType"]) > 0){
                this.calcStatus(status,num,i);
            }
        }


        for(var i in pazzle.colorMap){
            if(color1.indexOf(pazzle.colorMap[i]) > 0){
                color1.shift();
            }
            if(color2.indexOf(pazzle.colorMap[i]) > 0){
                color2.shift();
            }
        }

        if(color1.lenght <= 0
            && color2.lenght <= 0){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num3,i);
            }
        }else if(color.lenght <= 0){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num2,i);
            }
        }
    }

    /**
    *
    * 特定属性の組み合わせか、特定属性の組み合わせてステータスアップし
    * 特定属性の組み合わせの時にステータスが最大になる
    *
    * */
   ,type582:function(p){
       var color1 = p[0].split(':');
       var color2 = p[1].split(':');
       var color3 = p[2].split(':');
       var status1 = p[3].split(':');
       var status2 = p[4].split(':');
       var num1 = Number(p[5]);
       var num2 = Number(p[6]);

       for(var i in pazzle.colorMap){
           if(color1.indexOf(pazzle.colorMap[i]) > 0){
               color1.shift();
           }
           if(color2.indexOf(pazzle.colorMap[i]) > 0){
               color2.shift();
           }
           if(color3.indexOf(pazzle.colorMap[i]) > 0){
               color3.shift();
           }
       }

       if(color3.lenght <= 0){
           for(var i in data.PartieResult[0]){
               this.calcStatus(status2,num2,i);
           }
       }else if(color1.lenght <= 0
               || color2.lenght <= 0){
           for(var i in data.PartieResult[0]){
               this.calcStatus(status1,num1,i);
           }
       }
   }

   /**
    * 指定コンボ以上でステータスアップ、最大指定倍までステータスアップ
    * 特定属性のドロップを消すとステータスアップ
    * */
   ,type583:function(p){
       var count = Number(p[0]);
       var status = p[1].split(':');
       var num = Number(p[2]);
       var up =  Number(p[3]);
       var max =  Number(p[4]);
       var color = p[5].split(":");
       var status2 = p[6].split(":");
       var num2 = Number(p[7]);

       var colorflag = false;

       for(var c in pazzle.colorMap){
           if($.inArray(Number(pazzle.colorMap[c]), color) >= 0){
               colorflag = true;
           }
       }

       if(pazzle.comboCount >= count){

           var over = pazzle.comboCount - count;
           var n = num + (n * over);

           if(n >= max){
               n = max;
           }
           for(var i in data.PartieResult[0]){
               this.calcStatus(status,n,i);

               if(colorflag){
                   this.calcStatus(status2,num2,i);
               }
           }
       }
   }


    /**=======================================================
    *
    *
    * 		ドロップを指定個以上繋げると、ステータスアップ
    *
    *
    =========================================================*/

    /*指定色のドロップを指定子繋げると、ステータスアップ*/
    ,type601:function(p){
        var color = p[0].split(':');
        var count = Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);

        var c = false;

        for(var i in color){
            if($.inArray(Number(color[i]), pazzle.colorMap) >= 0){
                for(var j in pazzle.combo[data.colorMap[color[i]]]){
                    if(pazzle.combo[data.colorMap[color[i]]][j][0] >= count){
                        c = true;
                    }
                }
            }
        }

        if(c){
            for(var i in data.PartieResult[0]){
                this.calcStatus(status,num,i);
            }
        }
    }

    /*指定色のドロップを指定子繋げるとステータスアップし、一定倍率まであがる*/
    ,type602:function(p){
        var color = p[0].split(':');
        var count = Number(p[1]);
        var status = p[2].split(':');
        var num = Number(p[3]);
        var max = Number(p[4]);
        var up = Number(p[5]);

        var c = false;
        var top = 0;

        for(var i in color){
            if($.inArray(Number(color[i]), pazzle.colorMap) >= 0){
                for(var j in pazzle.combo[data.colorMap[color[i]]]){
                    if(pazzle.combo[data.colorMap[color[i]]][j][0] >= count){
                        c = true;
                        if(top < pazzle.combo[data.colorMap[color[i]]][j][0]){
                            top = pazzle.combo[data.colorMap[color[i]]][j][0];
                        }
                    }
                }
            }
        }

        if(c){

            if(top > max){
                top = max;
            }

            var n = num + (up * (top - count));

            for(var i in data.PartieResult[0]){
                this.calcStatus(status,n,i);
            }
        }
    }
    /**=======================================================
    *
    *
    * 		スキルを使用したターンステータスアップ
    *
    *
    =========================================================*/

    /*スキルを使用するとステータスアップ*/
    ,type701:function(p){

    }
    /*スキルを使用すると特定属性のステータスアップ*/
    ,type702:function(p){

    }
    /*スキルを使用すると特定タイプのステータスアップ*/
    ,type703:function(p){

    }

    /**=======================================================
    *
    *
    * 		特定のモンスターがいる場合ステータスアップ
    *
    *
    =========================================================*/
    /*
     * 特定のモンスターがいる場合ステータスアップ
     */
    ,type801:function(p){

    }

    /*
     * 特定のモンスターが全ている場合ステータスアップ
     */
    ,type802:function(p){

    }


    /**=======================================================
    *
    *
    * 		ダメージ軽減
    *
    *
    =========================================================*/
    /*
     *受けるダージを軽減
     */
    ,type901: function(p){
        var filter = Number(p[0]);
        var status = p[1].split(':');
        var num = Number(p[2]);

        this.HP_Now;
    }

    /*
     *特定属性から受けるダージを軽減
     */
    ,type902: function(p){
        var filter = Number(p[0]);
        var attribute = p[1].split(':');
        var status = p[2].split(':');
        var num = Number(p[3]);

        this.HP_Now;
    }

    /*
     *HPが特定条件時、受けるダージを軽減
     */
    ,type903: function(p){
        var filter = Number(p[0]);
        var status = p[1].split(':');
        var num = Number(p[2]);

        this.HP_Now;
    }

    /*
     * 特定属性のステータスが上がり、特定属性から受けるダージを軽減
     */
    ,type981: function(p){
        var a = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        for(var i in data.PartieResult[0]){
            var re = data.PartieResult[0][i];
            if(t.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0
                && t.indexOf(re["SubAttribute"]) > 0){

                this.calcStatus(s,n,i);
            }
        }
    }

    /*
     * 特定タイプのステータスが上がり、特定属性から受けるダージを軽減
     */
    ,type982: function(p){
        var t = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        for(var i in data.PartieResult[0]){
            var re = data.PartieResult[0][i];
            if(t.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && t.indexOf(re["SubType"]) > 0){

                this.calcStatus(s,n,i);
            }
        }
    }

    /*
     * 特定属性と特定タイプのステータスが上がり、特定属性から受けるダージを軽減
     */
    ,type983: function(p){
        var a = p[0].split(':');
        var t = p[1].split(':');
        var s = p[2].split(':');
        var n = Number(p[3]);

        for(var i in data.PartieResult[0]){
            var re = data.PartieResult[0][i];
            if(t.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && t.indexOf(re["SubType"] > 0)
                ||t.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0
                && t.indexOf(re["SubAttribute"]) > 0){

                this.calcStatus(s,n,i);
            }
        }
    }


    /**=======================================================
    *
    *
    * 		ドロップを消す回復
    *
    *
    =========================================================*/
    /*
     *ドロップを消すと　特定量HP回復
     */
    ,type1001: function(p){
        var filter = Number(p[0]);
        var num = Number(p[2]);
    }

    /*
     *ドロップを消すと　指定量HP回復
     */
    ,type1002: function(p){
        var filter = Number(p[0]);
        var num = Number(p[2]);
    }

    /*
     *特定属性のステータスが上がり、ドロップを消す特定量HP回復
     */
    ,type1081: function(p){
        var a = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        for(var i in data.PartieResult[0]){
            var re = data.PartieResult[0][i];
            if(t.indexOf(re["Attribute"]) > 0
                || re["SubAttribute"] > 0
                && t.indexOf(re["SubAttribute"]) > 0){

                this.calcStatus(s,n,i);
            }
        }Number(p[2]);
    }

    /*
     *特定タイプのステータスが上がり、ドロップを消す特定量HP回復
     */
    ,type1082: function(p){
        var t = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        for(var i in data.PartieResult[0]){
            var re = data.PartieResult[0][i];
            if(t.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && t.indexOf(re["SubType"]) > 0){

                this.calcStatus(s,n,i);
            }
        }
    }



    /**=======================================================
    *
    *
    * 		攻撃後追い打ち
    *
    *
    =========================================================*/
    /*
     *攻撃後、追加ダメージを与える
     */
    ,type1101:function(){

    }

    /**=======================================================
    *
    *
    * 		踏ん張る
    *
    *
    =========================================================*/
    /*
     *HPが特定量の時、即死ダメージを受けてもHPが1残る
     */
    ,type1201:function(){

    }



    /**=======================================================
    *
    *
    * 		反撃
    *
    *
    =========================================================*/
    /*
     *ダメージを受けると、特定属性で反撃する
     */
    ,type1301:function(){

    }


    /**=======================================================
    *
    *
    * 		ドロップ操作時間延長
    *
    *
    =========================================================*/
    /*
     *ドロップ操作時間が特定秒伸びる
     */
    ,type1401:function(){

    }
    /*
     * ドロップ操作時間が特定秒伸びる
     * 特定タイプのステータスアップ
     */
    ,type1402:function(){
        var t = p[0].split(':');
        var s = p[1].split(':');
        var n = Number(p[2]);

        for(var i in data.PartieResult[0]){
            var re = data.PartieResult[0][i];
            if(t.indexOf(re["Type"]) > 0
                || re["SubType"] > 0
                && t.indexOf(re["SubType"]) > 0){

                this.calcStatus(s,n,i);
            }
        }
    }
    /**=======================================================
    *
    *
    * 		入手コインが増える（サブと入れ替え不可）
    *
    *
    =========================================================*/
    /*
     *入手コインが増える
     */
    ,type1501:function(){

    }
    /**=======================================================
    *
    *
    * 		パワーアップ合成時特殊効果
    *
    *
    =========================================================*/
    /*
     *合成時特殊効果
     */
    ,type1601:function(){

    }
    /**=======================================================
    *
    *
    * 		ドロップ操作時、特殊効果
    *
    *
    =========================================================*/
    /*
     *ドロップ操作時特殊効果
     */
    ,type1701:function(){

    }
}