
var dropmap = {

    hoge:""

    ,setuped : false

    ,setup: function(){
        this.showSkillList();
        if(!this.setuped){
            this.setFilterEvent();
            this.setuped = true;
        }
    }

    ,showSkillList: function(){

        $("#dropmap-box").empty();

        for(var i in data.dropChengeMap){
            var dm = data.dropChengeMap[i];
            if(i != 0){
                if(this.dmapFilter[i] == undefined){
                    this.dmapFilter[i] = 0;
                }

                if(this.dmapFilter[i] < 1){
                     $("#dropmap-box").append("" +
                            "<li>" +
                            " <div>" +
                            " 	<p class='dm-name'>"+skill.skills[dm["id"]]["Name"]+"</p>" +
                            " 	<p class='dm-skill'>"+skill.skills[dm["id"]]["Description"]+"</p>" +
                            " 	<p class='dm-start'>初期："+skill.skills[dm["id"]]["Start"]+"</p>" +
                            " 	<p class='dm-max'>最大："+skill.skills[dm["id"]]["Max"]+"</p>" +
                            " </div>" +
                            " <div>" + this.buildDropChenge(i)+"<div class='dm-monster-list'>"+this.buildMonsterList(i)+"</div>" +
                            " </div>" +
                            "</li>");
                }
            }
        }

        return function(){
            dropmap.setViewEvent();
        }();
    }

    ,afilter:[0,0,0,0,0,0,0,0,0,0,0,0,0]
    ,bfilter:[0,0,0,0,0,0,0,0,0]
    ,dmapFilter:[]

    ,setFilterEvent: function(){
        $("#dm-bsearch > li").each(function(){
            $(this).bind("click",function(){
                var n = $(this).index() + 1;
                if(!dropmap.bfilter[n]){
                    $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                    dropmap.bfilter[n] = 1;
                    dropmap.filterList(0,n,0);
                }else{
                    $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                    dropmap.bfilter[n] = 0;
                    dropmap.filterList(0,n,1);
                }
            })
        })
        $("#dm-asearch > li").each(function(){
            $(this).bind("click",function(){
                var n = $(this).index() + 1;
                if(!dropmap.afilter[n]){
                    $(this).css({backgroundColor:"rgba(220,150,100,0.6)"});
                    dropmap.afilter[n] = 1;
                    dropmap.filterList(1,n,0);
                }else{
                    $(this).css({backgroundColor:"rgba(150,150,150,0.1)"});
                    dropmap.afilter[n] = 0;
                    dropmap.filterList(1,n,1);
                }
            })
        })
    }
    ,filterList: function(val,num,s){
        for(var i in data.dropChengeMap){

            if(i != 0){
                if(val){
                    var fil = data.dropChengeMap[i]["bchenge"].split(":");
                }else{
                    var fil = data.dropChengeMap[i]["achenge"].split(":");
                }

                if(fil.indexOf(String(num)) < 0){
                    if(s){
                        this.dmapFilter[i]--;
                    }else{
                        this.dmapFilter[i]++;
                    }
                }
            }
        }

        return function(){
            dropmap.showSkillList();
        }();
    }


    ,buildDropChenge: function(num){
        var dd = data.dropChengeMap[num];
        var ad = dd["achenge"].split(":");
        var bd = dd["bchenge"].split(":");

        var dlist = function(d){
            var dl = "";
            for(var i in d){
                if(d[i] != 0 && d[i] <= 8){
                    dl = dl + "<p class='"+data.colorMap[d[i]]+"'></p>";
                }
            }
            return dl;
        }


        var dma = "";
        var dmb = "";
        var dc = data.colorMap;

        //単色変換
        if(dd["type"] == "1"){
            dmb = "<p class='"+dc[bd[1]]+"'></p>";
            dma = "<p class='"+dc[ad[1]]+"'></p>";
        //多色単色変換
        }else if(dd["type"] == "2"){
            dmb = dlist(bd);
            dma = "<p class='"+dc[Number(ad[1])]+"'></p>";

        //多色多色変換
        }else if(dd["type"] == "3"){
            return "<p class='"+dc[bd[0]]+"'></p>"+
            "<p class='dm-arrow'></p>"+
            "<p class='"+dc[ad[0]]+"'></p>"+
            "<p></p>"+
            "<p class='"+dc[bd[1]]+"'></p>"+
            "<p class='dm-arrow'></p>"+
            "<p class='"+dc[ad[1]]+"'></p>";

        //全て多色変換
        }else if(dd["type"] == "4"){
            dmb = "<p class='dm-all'>全て</p>";
            dma = dlist(ad);
        //横一列変換
        }else if(dd["type"] == "5"){
            return "<p>横一列</p>" +
                    "<p class='dm-arrow'></p>"+
                    "<p class='"+dc[Number(ad[1])]+"'></p>";
        //縦一列変換
        }else if(dd["type"] == "6"){
            return "<p>縦一列</p>" +
            "<p class='dm-arrow'></p>"+
            "<p class='"+dc[Number(ad[1])]+"'></p>";
        //入れ替え
        }else if(dd["type"] == "7"){
            return "<div class='dm-refresh'>総入れ替え</div>";;
        }

        return dmb+"<p class='dm-arrow'></p>"+dma;
    }

    ,buildMonsterList: function(num){
        var dd = data.dropChengeMap[num];

        var check = true;
        var st = 0;
        var list = "";

        while(check){

            var mons = this.showMonster(dd["id"],st);

            if(mons != false){
                list = list + "<p class='mimage-s' data-role='"+mons+"' ><img src='img/m/"+mons+".jpg'></p>"
                st = mons;
            }else{
                check = false;
            }
        }

        return list;
    }

    ,showMonster: function(sk,num){
        var n = monster.skillMap.indexOf(sk,num);
        if(n > 0){
            return n + 1;
        }else{
            return false;
        }
    }

    ,setViewEvent: function(){
        $("ul#dropmap-box li").find(".dm-monster-list").each(function(){
            $(this).find("p").each(function(){
                $(this).bind("click",function(){
                    monster.buildView(this);
                })
            })
        })
    }

}
