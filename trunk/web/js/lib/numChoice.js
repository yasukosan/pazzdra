var numchoice = {

    Target:""
    ,viewTarget:"body"
    ,Num:""
    ,ReturnFunc:{}
    ,View:false
    ,Already:false
    ,event: "click"
    ,Cell: 0

    ,setup:function(target,rfunction,num){

        if(!this.View){
            this.Target = target;
            this.Num = "";
            this.buildChoice();
            this.setEvent();
            this.ReturnFunc = rfunction;
            this.View = true;
            this.Cell = num;

            if($(this.Target).val() != ""){
                this.Num = Number($(this.Target).val());
                this.viewReset();
            }
        }else if(this.Target != target){
            this.close();
            this.setup(target, viewTarget, rfunction);
        }
    }

    ,addChoice:function(){

    }

    ,buildChoice:function(){

        $(this.viewTarget).append(this.tmpl());
        $('#numchoice').show();
        $('p#numchoice-num').append($(this.Target).html());

    }

    ,tmpl:function(){

        return 	"" +
            "<dl id='numchoice-screen'></div>" +
            "<dl id='numchoice'>" +
            "	<dd id='numchoice-box'>" +
            "		<ul>" +
            "			<li>1</li><li>2</li><li>3</li>" +
            "			<li>4</li><li>5</li><li>6</li><li>7</li>" +
            "			<li>8</li><li>9</li><li>0</li><li>99</li>" +
            "		</ul>" +
            "	</dd>" +
            "	<dd id='numchoice-view'>" +
            "		<p id='numchoice-num'> </p>" +
            "		<p id='numchoice-del'>消す</p>" +
            "		<p id='numchoice-submit'>決定</p>" +
            "		<p id='numchoice-close'>閉じる</p>" +
            "	</dd>" +
            "</dl>";

    }

    ,setEvent:function(){

        numchoice.event = "click";


        $("#numchoice-box").find("li").each(function(i){
            if(i == 9){
                var num = 0;
            }else if(i==10){
                var num = 99;
            }else{
                var num = i + 1;
            }
            $(this).bind(numchoice.event,function(){
                numchoice.addNum(this,num);
            });
        });

        $("#numchoice-del").bind(numchoice.event,function(){
            numchoice.removeNum();
        });
        $("#numchoice-submit").bind(numchoice.event,function(){
            numchoice.sendNum();
        });
        $("#numchoice-close").bind(numchoice.event,function(){
            numchoice.close();
        });
    }

    ,addNum:function(target,i){
        if(this.Num == 0){
            this.Num = ""+i;
        }else{
            this.Num += i;
        }
        this.viewReset();
    }

    ,removeNum:function(){
        this.Num = this.Num.slice(0,-1);
        this.viewReset();
    }

    ,sendNum:function(){
        if(this.ReturnFunc !== "" && typeof(this.ReturnFunc) == "function"){
            this.ReturnFunc(this.Num);
        }else{

            $(this.Target).empty();
            if(this.Num <= 0){
                $(this.Target).append(0);
            }else{
                $(this.Target).append(this.Num);
            }
            $(this.Target).blur();
            this.close();
        }

        partie.setPlusStatus(this.Cell);
    }

    ,close:function(){
        numchoice.View = false;
        numchoice.Already = false;
        $("#numchoice").remove();
        $("#numchoice-screen").remove();
    }

    ,viewReset:function(){
        $("#numchoice-num").empty();
        $("#numchoice-num").append(this.Num);
    }

}